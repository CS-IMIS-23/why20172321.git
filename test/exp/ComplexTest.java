package exp;

import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a = new Complex(3, 1);
    Complex b = new Complex(3, 4);
    Complex c = new Complex(1, 2);
    Complex d = new Complex(3, 4);

    @Test
    public void testComplexAdd() {
        assertEquals(new Complex(7, -2).toString(), a.ComplexAdd(new Complex(4, -3)).toString());
        assertEquals(new Complex(3, 4).toString(), a.ComplexAdd(new Complex(0, 3)).toString());
        assertEquals(new Complex(4, 1).toString(), a.ComplexAdd(new Complex(1, 0)).toString());
        assertEquals(new Complex(3, 1).toString(), a.ComplexAdd(new Complex(0, 0)).toString());
        assertEquals(new Complex(-6, 0).toString(), a.ComplexAdd(new Complex(-9, -1)).toString());
        assertEquals(new Complex(4, -1).toString(), a.ComplexAdd(new Complex(1, -2)).toString());
        assertEquals(new Complex(0, -2).toString(), a.ComplexAdd(new Complex(-3, -3)).toString());
    }

    @Test
    public void testComplexSub() {
        assertEquals(new Complex(1, 0).toString(), b.ComplexSub(new Complex(2, 4)).toString());
        assertEquals(new Complex(3, 3).toString(), b.ComplexSub(new Complex(0, 1)).toString());
        assertEquals(new Complex(2, 4).toString(), b.ComplexSub(new Complex(1, 0)).toString());
        assertEquals(new Complex(3, 4).toString(), b.ComplexSub(new Complex(0, 0)).toString());
        assertEquals(new Complex(5, 3).toString(), b.ComplexSub(new Complex(-2, 1)).toString());
        assertEquals(new Complex(2, 6).toString(), b.ComplexSub(new Complex(1, -2)).toString());
        assertEquals(new Complex(6, 7).toString(), b.ComplexSub(new Complex(-3, -3)).toString());
    }

    @Test
    public void testComplexMulti() {
        assertEquals(new Complex(-6, 8).toString(), c.ComplexMulti(new Complex(2, 4)).toString());
        assertEquals(new Complex(-2, 1).toString(), c.ComplexMulti(new Complex(0, 1)).toString());
        assertEquals(new Complex(1, 2).toString(), c.ComplexMulti(new Complex(1, 0)).toString());
        assertEquals(new Complex(0, 0).toString(), c.ComplexMulti(new Complex(0, 0)).toString());
        assertEquals(new Complex(-4, -3).toString(), c.ComplexMulti(new Complex(-2, 1)).toString());
        assertEquals(new Complex(5, 0).toString(), c.ComplexMulti(new Complex(1, -2)).toString());
        assertEquals(new Complex(3, -9).toString(), c.ComplexMulti(new Complex(-3, -3)).toString());
    }

    @Test
    public void testComplexDiv() {
        assertEquals(new Complex(1.1, -0.2).toString(), d.ComplexDiv(new Complex(2, 4)).toString());
        assertEquals(new Complex(4, -3).toString(), d.ComplexDiv(new Complex(0, 1)).toString());
        assertEquals(new Complex(3, 4).toString(), d.ComplexDiv(new Complex(1, 0)).toString());
        assertEquals(new Complex(-0.4, -2.2).toString(), d.ComplexDiv(new Complex(-2, 1)).toString());
        assertEquals(new Complex(-1, 2).toString(), d.ComplexDiv(new Complex(1, -2)).toString());
        assertEquals(new Complex(-1.3, -0.9).toString(), d.ComplexDiv(new Complex(-3, -1)).toString());
    }


}
