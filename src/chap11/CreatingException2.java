package chap11;

import java.util.Scanner;

public class CreatingException2 {
    public static void main(String[] args) throws StringTooLongException {
        final int MAX = 20;
        Scanner scanner = new Scanner(System.in);

        StringTooLongException problem = new StringTooLongException("Input zifuchuan is string too long");

        System.out.println("Enter a zifuchuan :");
        String zifuchuan = "";
        String zifu = "";
        while (true) {
            zifu = scanner.nextLine();
            if (zifu.equalsIgnoreCase("DONE")) {
                break;
            }
            zifuchuan += zifu;
        }
        System.out.println("jieguo: " + zifuchuan);
        try {
            if (zifuchuan.length() > MAX)
                throw problem;
        } catch (StringTooLongException exception) {
            System.out.println("The input string is too long.");
        }
        System.out.println("End of main methed.");
    }
}
