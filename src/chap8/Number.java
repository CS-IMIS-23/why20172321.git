//**************************************************
// Number.java   Author:why
//
//  读取0~50内多个整数，并输出各种出现次数
//**************************************************
import java.util.Scanner;

public class Number
{
	public static void main(String[] args)
	{
	final int NUM = 51;

	int[] upp = new int[NUM];
	int num;

	do	
	{
	Scanner scan = new Scanner(System.in);
	System.out.println("Enter0~50:");
	num = scan.nextInt();

	if ( num >= 0 && num <= 50)
	upp[num] ++;
	else
	break;
	}
	while ( num >= 0 && num <= 50);

	for (int i = 0; i < upp.length; i ++)
	{
	System.out.println(i + ":" + upp[i] + "\t");
	}

	}
}


	
