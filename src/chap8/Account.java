//**********************************************************
// Account.java  Author:改编例题4.4
//
//**********************************************************

import java.text.NumberFormat;

public class Account
{
    private final double RATE = 0.03;//似乎就是把书上的3.5%改成3%

    private long acctNumber;
    private double balance;
    private String name;

    //-----------------------------------------------------------
    // Sets up the account by defining its owner, aacount number,
    // and inital balance
    //-----------------------------------------------------------
    public Account(String owner, long account, double initial)
    {
        name = owner;
        acctNumber = account;
        balance = initial;
    }

    //--------------------------------------------------------------
    // Deposits thespeciafied amount into the account. Returns the
    // new balance
    //--------------------------------------------------------------
    public double deposit(double amount)
    {
        balance = balance + amount;
        return balance;
    }

    //--------------------------------------------------------------
    // Withdraws the specified amount from the account and applies
    // the fee. Returns the new balance
    //--------------------------------------------------------------
    public double withdraw(double amount, double fee)
    {
        balance = balance - amount - fee;

        return balance;
    }

    //---------------------------------------------------------------
    // Adds interest to the account and returns the new balance
    //---------------------------------------------------------------
    public double addInterest()
    {
        balance += (balance * RATE);
        return balance;
    }

    //---------------------------------------------------------------
    // Returns the current balance of the account
    //---------------------------------------------------------------
    public double getBalance()
    {
        return balance;
    }

    //----------------------------------------------------------------
    // Returns a one-line description of the account as a string;
    //----------------------------------------------------------------
    public String toString()
    {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        return acctNumber + "\t" + name + "\t" + fmt.format(balance);
    }
}
