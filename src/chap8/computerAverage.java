package chap8;

public class computerAverage {
    public static void main(String... args) {
        double sum = 0;
        for (int i = 0; i < args.length - 1; i++)
            sum += Integer.parseInt(args[i + 1]);

        double average = sum / (args.length - 1);

        System.out.println(args[0] + " average score : " + average);
    }
}
