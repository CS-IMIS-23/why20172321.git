package chap12;

import java.util.*;

class PalindromeTester {
    public int left, right;

    public PalindromeTester(String str) {
        left = 0;
        right = str.length() - 1;
    }

    public void digui(String str) {
        while (str.charAt(left) == str.charAt(right) && left < right) {
            left++;
            right--;
            digui(str);
        }
    }

    public void panduan() {
        if (left < right)
            System.out.println("That string is NOT a palindrome.");
        else
            System.out.println("That string is a palindrome.");
    }

    //---------------------------------------------------------------------------------------------------
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str, another = "y";
        while (another.equalsIgnoreCase("y")) {
            System.out.println("Enter a potential palidrome.");
            str = scan.nextLine();

            PalindromeTester palindromeTester = new PalindromeTester(str);
            palindromeTester.digui(str);
            palindromeTester.panduan();
            System.out.println();
            System.out.println("Test another palindrom(y/n)");
            another = scan.nextLine();
        }
    }
}
