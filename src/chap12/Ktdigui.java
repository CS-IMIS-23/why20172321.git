package chap12;

import java.io.*;
import java.util.Scanner;

public class Ktdigui {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入一个整数：");
        int n;
        n = scanner.nextInt();

        int result = fact(n);
        System.out.println("F(n) = " + result);
    }

    static int fact(int n) {
        if (n == 0)
            return 0;
        else if (n == 1)
            return 1;
        else
            return fact(n - 1) + fact(n - 2);
    }
}

