package chap12;

import java.util.*;

public class Pascal {
    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("需要输出Pascal三角形第几行：");
        int n = scanner.nextInt();

        int num[][] = new int[n][n];
        for (int i = 0; i < n; i++) {
            num[i][0] = num[i][i] = 1;
            for (int j = 1; j < i; j++) {
                num[i][j] = num[i - 1][j - 1] + num[i - 1][j];
            }
        }

        for (int j = 0; j <= n - 1; j++) {
            System.out.print(num[n - 1][j] + " ");
        }

    }
}
