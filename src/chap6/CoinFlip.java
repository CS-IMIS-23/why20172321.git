//***************************************
//  CoinFlip.java   Author:why
//
//	demonstrates the  use of an if-else statement and a for-loop.
//***************************************

public class CoinFlip
{
	//------------------------------------
	//creates a Coin Object, flips it, and prints the results.
	//------------------------------------------

	public static void main(String[] args)
	{
	Coin myCoin = new Coin();

	
	final int C = 100;
	int H = 0, T = 0;

	for (int count = 0; count < C; count ++)
	{
	myCoin.flip();
	if (myCoin.isHeads())
	H ++;
	else
	T ++;
	}

	System.out.println("HEADS:" + H);
	System.out.println("Tails:" + T);

	}
}











