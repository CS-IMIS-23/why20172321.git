package week9.课本代码.jsjf;

public class ArrayHeaptest {
    public static void main(String[] args) {
        ArrayHeap arrayHeap = new ArrayHeap();
        arrayHeap.addElement1(36);
        arrayHeap.addElement1(30);
        arrayHeap.addElement1(18);
        arrayHeap.addElement1(40);
        arrayHeap.addElement1(32);
        arrayHeap.addElement1(45);
        arrayHeap.addElement1(22);
        arrayHeap.addElement1(50);
        System.out.println("大顶堆树层序输出" + arrayHeap.levelOrder());

        int[] array = new int[8];
        int index = 0;
        while (!arrayHeap.isEmpty()) {
            int temp = 0;
            temp = (int) arrayHeap.removeMax();
            array[index] = temp;
            index++;
            System.out.print("第" + index + "轮排序为： ");
            for (int i = 0; i < index; i++) {
                System.out.print(array[i] + " ");
            }
            System.out.print(arrayHeap.levelOrder());
            System.out.println(" ");
        }
    }
}
