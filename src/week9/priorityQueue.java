package week9;

import java.io.IOException;

//pp12.1

class priorityQueue {
    private int maxSize;
    private long[] queArray;
    private int nItems;

    public priorityQueue(int s) {
        maxSize = s;
        queArray = new long[maxSize];
        nItems = 0;
    }

    public void insert(long item) {
        int j;
        if (nItems == 0)
            queArray[nItems++] = item;
        else {
            for (j = nItems - 1; j >= 0; j--) {
                if (item > queArray[j])
                    queArray[j + 1] = queArray[j];
                else
                    break;
            }
            queArray[j + 1] = item;
            nItems++;
        }
    }

    public long remove() {
        return queArray[--nItems];
    }

    public long peekMin() {
        return queArray[nItems - 1];
    }

    public boolean isEmpty() {
        return (nItems == 0);
    }

    public boolean isFull() {
        return (nItems == maxSize);
    }
}

class PriorityQueueTest {
    public static void main(String[] args) throws IOException {
        priorityQueue thePQ = new priorityQueue(5);
        thePQ.insert(33);
        thePQ.insert(30);
        thePQ.insert(32);
        thePQ.insert(31);
        while (!thePQ.isEmpty()) {
            long item = thePQ.remove();
            System.out.println(item + "");
        }
        System.out.println(" ");
    }
}