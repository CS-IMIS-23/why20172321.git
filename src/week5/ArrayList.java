package week5;

import week5.书上代码.jsjf.ListADT;
import week5.书上代码.jsjf.exceptions.ElementNotFoundException;
import week5.书上代码.jsjf.exceptions.EmptyCollectionException;

import java.util.*;

public abstract class ArrayList<T> implements ListADT<T>, Iterable<T> {
    private final static int DEFAULT_CAPACITY = 100;
    private final static int NOT_FOUND = -1;

    protected int rear;
    protected T[] list;
    protected int modCount;

    public ArrayList() {
        this(DEFAULT_CAPACITY);
    }

    public ArrayList(int initialCapacity) {
        rear = 0;
        list = (T[]) (new Object[initialCapacity]);
        modCount = 0;
    }

    //扩展容量
    protected void expandCapacity() {
        T[] larger = (T[]) (new Object[list.length * 2]);
        for (int scan = 0; scan < modCount; scan++) {
            larger[scan] = list[rear];
            rear = (rear + 1) % list.length;
        }
        rear = 0;
        rear = modCount;
        list = larger;
    }

    //删除最后一个元素
    public T removeLast() throws EmptyCollectionException {
        T result = list[rear - 1];
        list[rear] = null;
        rear--;
        return result;
    }

    //删除第一个元素
    public T removeFirst() throws EmptyCollectionException {
        T result = list[0];
        remove(result);
        return result;
    }

    //删除某个元素
    public T remove(T element) {
        T result;
        int index = find(element);

        if (index == NOT_FOUND)
            throw new ElementNotFoundException("ArrayList");

        result = list[index];
        rear--;

        // shift the appropriate elements
        for (int scan = index; scan < rear; scan++)
            list[scan] = list[scan + 1];

        list[rear] = null;
        modCount++;

        return result;
    }

    //查看前端元素
    public T first() throws EmptyCollectionException {
        // To be completed as a Programming Project
        return list[0];
    }

    //查看末端元素
    public T last() throws EmptyCollectionException {
        // To be completed as a Programming Project
        return list[rear - 1];
    }

    //是否有某个元素
    public boolean contains(T target) {
        return (find(target) != NOT_FOUND);
    }

    //查找某个元素
    private int find(T target) {
        int scan = 0;
        int result = NOT_FOUND;

        if (!isEmpty())
            while (result == NOT_FOUND && scan < rear)
                if (target.equals(list[scan]))
                    result = scan;
                else
                    scan++;

        return result;
    }

    //是否为空
    public boolean isEmpty() {
        // To be completed as a Programming Project
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    //元素数量
    public int size() {
        // To be completed as a Programming Project
        return rear;
    }

    //输出
    public String toString() {
        // To be completed as a Programming Project
        String last = "";
        for (int a = 0; a < size(); a++) {
            last += list[a] + " ";
        }
        return last;
    }


    public Iterator<T> iterator() {
        return new ArrayListIterator();
    }


    private class ArrayListIterator implements Iterator<T> {
        int iteratorModCount;
        int current;


        public ArrayListIterator() {
            iteratorModCount = modCount;
            current = 0;
        }


        public boolean hasNext() throws ConcurrentModificationException {
            if (iteratorModCount != modCount)
                throw new ConcurrentModificationException();

            return (current < rear);
        }

        public T next() throws ConcurrentModificationException {
            if (!hasNext())
                throw new NoSuchElementException();

            current++;

            return list[current - 1];
        }

        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }

    }
}
