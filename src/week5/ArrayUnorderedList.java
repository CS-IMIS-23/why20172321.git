package week5;


import week5.ArrayList;
import week5.书上代码.jsjf.UnorderedListADT;
import week5.书上代码.jsjf.exceptions.ElementNotFoundException;


public class ArrayUnorderedList<T> extends ArrayList<T>
        implements UnorderedListADT<T> {

    public ArrayUnorderedList() {
        super();
    }


    public ArrayUnorderedList(int initialCapacity) {
        super(initialCapacity);
    }


    public void addToFront(T element) {
        for (int i = rear; i > -1; i--) {
            list[i + 1] = list[i];

        }
        list[0] = element;
        rear++;
    }


    public void addToRear(T element) {
        list[rear] = element;
        rear++;
    }


    public void addAfter(T element, T target) {
        if (size() == list.length)
            expandCapacity();

        int scan = 0;

        // find the insertion point
        while (scan < rear && !target.equals(list[scan]))
            scan++;

        if (scan == rear)
            throw new ElementNotFoundException("UnorderedList");

        scan++;

        // shift elements up one
        for (int shift = rear; shift > scan; shift--)
            list[shift] = list[shift - 1];

        // insert element
        list[scan] = element;
        rear++;
        modCount++;
    }
}
