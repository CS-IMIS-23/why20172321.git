package week5;

public class AOLTest extends ArrayOrderedList {
    public static void main(String[] args) {
        AOLTest aolTest = new AOLTest();
        aolTest.add(2);
        aolTest.add(1);
        aolTest.add(3);
        aolTest.add(5);
        System.out.println("列表是否为空：" + aolTest.isEmpty());
        System.out.println("列表内容：" + aolTest.toString());
        System.out.println("列表头为：" + aolTest.first());
        System.out.println("列表尾为：" + aolTest.last());
        System.out.println("列表长度：" + aolTest.size());
        aolTest.removeFirst();
        aolTest.removeLast();
        System.out.println("列表内容：" + aolTest.toString());
        System.out.println("列表长度：" + aolTest.size());
        System.out.println("是否有1：" + aolTest.contains(1));
        System.out.println("是否有2：" + aolTest.contains(2));
    }
}
