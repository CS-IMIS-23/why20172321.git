package week5;


import week5.LinkedList;
import week5.书上代码.jsjf.OrderedListADT;
import week5.书上代码.jsjf.exceptions.NonComparableElementException;


public class LinkedOrderedList<T> extends LinkedList<T>
        implements OrderedListADT<T> {
    /**
     * Creates an empty list.
     */
    public LinkedOrderedList() {
        super();
    }

    /**
     * Adds the specified element to this list at the location determined by
     * the element's natural ordering. Throws a NonComparableElementException
     * if the element is not comparable.
     *
     * @param element the element to be added to this list
     * @throws NonComparableElementException if the element is not comparable
     */
    public void add(T element) {
        LinearNode<T> temp = new LinearNode(element);
        LinearNode<T> previous = null;
        LinearNode<T> current = head;
        while (current != null && Integer.parseInt(element + "") > Integer.parseInt(current.getElement() + "")) {
            previous = current;
            current = current.getNext();
        }
        if (previous == null) {
            head = tail = temp;
        } else {
            previous.setNext(temp);
        }
        temp.setNext(current);
        if (temp.getNext() == null)
            tail = temp;

        count++;
        modCount++;

    }
}
