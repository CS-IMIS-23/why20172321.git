package week5;

public class AULTest extends ArrayUnorderedList {
    public static void main(String[] args) {
        AULTest aulTest = new AULTest();
        aulTest.addToFront(2);
        aulTest.addToRear(1);
        aulTest.addToFront(3);
        aulTest.addAfter(5, 3);
        System.out.println("列表内容：" + aulTest.toString());
        System.out.println("列表头为：" + aulTest.first());
        System.out.println("列表尾为：" + aulTest.last());
        System.out.println("列表长度：" + aulTest.size());
        aulTest.remove(2);
        System.out.println("是否有1：" + aulTest.contains(1));
        System.out.println("是否有2：" + aulTest.contains(2));
        aulTest.removeFirst();
        aulTest.removeLast();
        System.out.println("列表内容：" + aulTest.toString());
    }
}
