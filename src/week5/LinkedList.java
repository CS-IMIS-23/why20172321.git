package week5;

import week5.LinearNode;
import week5.书上代码.jsjf.ListADT;
import week5.书上代码.jsjf.exceptions.ElementNotFoundException;
import week5.书上代码.jsjf.exceptions.EmptyCollectionException;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;


public abstract class LinkedList<T> implements ListADT<T>, Iterable<T> {
    protected int count;
    protected LinearNode<T> head, tail;
    protected int modCount;

    /**
     * Creates an empty list.
     */
    public LinkedList() {
        count = 0;
        head = tail = null;
        modCount = 0;
    }


    public T removeFirst() throws EmptyCollectionException {
        // To be completed as a Programming Project
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        LinearNode<T> first = head;
        head = head.getNext();
        T result = first.getElement();

        count--;
        modCount++;
        return result;
    }

    public T removeLast() throws EmptyCollectionException {
        // To be completed as a Programming Project
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode<T> last = head;
        for (int i = 0; i < count - 1; i++) {
            last = last.getNext();
        }
        count--;
        modCount++;
        return last.getElement();

    }


    public T remove(T targetElement) throws EmptyCollectionException,
            ElementNotFoundException {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1)  // only one element in the list
            head = tail = null;
        else if (current.equals(head))  // target is at the head 
            head = current.getNext();
        else if (current.equals(tail))  // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        } else  // target is in the middle
            previous.setNext(current.getNext());

        count--;
        modCount++;

        return current.getElement();
    }


    public T first() throws EmptyCollectionException {
        // To be completed as a Programming Project
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        return head.getElement();
    }


    public T last() throws EmptyCollectionException {
        // To be completed as a Programming Project
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        return tail.getElement();
    }


    public boolean contains(T targetElement) throws
            EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            return false;
        else
            return true;
    }


    public boolean isEmpty() {
        // To be completed as a Programming Project
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }


    public int size() {
        // To be completed as a Programming Project
        return count;
    }

    public String toString() {
        String result = "";
        int n = count;
        LinearNode<T> node = head;
        while (n > 0) {
            result += node.getElement() + " ";
            node = node.getNext();
            n--;
        }
        return result;
    }


    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }

    /**
     * LinkedIterator represents an iterator for a linked list of linear nodes.
     */
    private class LinkedListIterator implements Iterator<T> {
        private int iteratorModCount;  // the number of elements in the collection
        private LinearNode<T> current;  // the current position


        public LinkedListIterator() {
            current = head;
            iteratorModCount = modCount;
        }


        public boolean hasNext() throws ConcurrentModificationException {
            if (iteratorModCount != modCount)
                throw new ConcurrentModificationException();

            return (current != null);
        }


        public T next() throws ConcurrentModificationException {
            if (!hasNext())
                throw new NoSuchElementException();

            T result = current.getElement();
            current = current.getNext();
            return result;
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        public void remove() throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }
    }

}


