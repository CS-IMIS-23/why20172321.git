package week5;


import week5.LinkedList;
import week5.书上代码.jsjf.UnorderedListADT;
import week5.书上代码.jsjf.exceptions.ElementNotFoundException;


public class LinkedUnorderedList<T> extends LinkedList<T>
        implements UnorderedListADT<T> {
    /**
     * Creates an empty list.
     */
    public LinkedUnorderedList() {
        super();
    }

    /**
     * Adds the specified element to the front of this list.
     *
     * @param element the element to be added to the list
     */
    public void addToFront(T element) {
        LinearNode<T> node = new LinearNode<T>((T) element);
        if (count == 0) {
            head = node;
            tail = node;
        } else {
            node.setNext(head);
            head = node;
        }
        count++;
    }

    /**
     * Adds the specified element to the rear of this list.
     *
     * @param element the element to be added to the list
     */
    public void addToRear(T element) {
        // To be completed as a Programming Project
        LinearNode<T> temp = new LinearNode<>(element);
        if (count == 0)
            head = tail = temp;
        else {
            tail.setNext(temp);
            tail = temp;
        }
        count++;
    }


    public void addAfter(T element, T target) {
        // To be completed as a Programming Project

        LinearNode<T> node = new LinearNode<>(element);
        LinearNode<T> current = head;
        LinearNode<T> temp = current.getNext();
        while (current.getElement() != target)
            current = current.getNext();
        node.setNext(current.getNext());
        current.setNext(node);
        if (node.getNext() == null)
            tail = node;
        count++;
    }
}
