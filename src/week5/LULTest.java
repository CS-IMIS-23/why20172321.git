package week5;

public class LULTest extends LinkedUnorderedList {
    public static void main(String[] args) {
        LULTest lulTest = new LULTest();
        lulTest.addToFront(2);
        lulTest.addToRear(1);
        lulTest.addToFront(3);
        lulTest.addAfter(5, 3);
        System.out.println("列表内容：" + lulTest.toString());
        System.out.println("列表头为：" + lulTest.first());
        System.out.println("列表尾为：" + lulTest.last());
        System.out.println("列表长度：" + lulTest.size());
        lulTest.remove(2);
        System.out.println("是否有1：" + lulTest.contains(1));
        System.out.println("是否有2：" + lulTest.contains(2));
        lulTest.removeFirst();
        lulTest.removeLast();
        System.out.println("列表内容：" + lulTest.toString());
    }
}
