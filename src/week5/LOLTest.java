package week5;

public class LOLTest extends LinkedOrderedList {
    public static void main(String[] args) {
        LOLTest lolTest = new LOLTest();
        lolTest.add(2);
        lolTest.add(1);
        lolTest.add(3);
        lolTest.add(5);
        System.out.println("列表是否为空：" + lolTest.isEmpty());
        System.out.println("列表内容：" + lolTest.toString());
        System.out.println("列表头为：" + lolTest.first());
        System.out.println("列表尾为：" + lolTest.last());
        System.out.println("列表长度：" + lolTest.size());
        lolTest.removeFirst();
        lolTest.removeLast();
        System.out.println("列表内容：" + lolTest.toString());
        System.out.println("列表长度：" + lolTest.size());
        System.out.println("是否有1：" + lolTest.contains(1));
        System.out.println("是否有2：" + lolTest.contains(2));
    }
}
