package week5.商品类;

import week2.EmptyCollectionException;
import week5.书上代码.jsjf.exceptions.ElementNotFoundException;

public class ProductList<T> {
    protected int count;
    protected ProductNode head, tail;
    protected int modCount;


    public ProductList() {
        count = 0;
        head = tail = null;
        modCount = 0;
    }

    public Product removeFirst() throws EmptyCollectionException {
        // To be completed as a Programming Project
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        ProductNode<Product> first = head;
        head = head.getNext();
        Product result = first.getElement();

        count--;
        modCount++;
        return result;
    }

    public Product removeLast() throws EmptyCollectionException {
        // To be completed as a Programming Project
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        ProductNode<Product> last = head;
        for (int i = 0; i < count - 1; i++) {
            last = last.getNext();
        }
        count--;
        modCount++;
        return last.getElement();
    }

    public Product remove(Product targetElement) throws EmptyCollectionException,
            ElementNotFoundException {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        ProductNode<Product> previous = null;
        ProductNode<Product> current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1)  // only one element in the list
            head = tail = null;
        else if (current.equals(head))  // target is at the head
            head = current.getNext();
        else if (current.equals(tail))  // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        } else  // target is in the middle
            previous.setNext(current.getNext());

        count--;
        modCount++;

        return current.getElement();
    }

    public Product first() throws EmptyCollectionException {
        // To be completed as a Programming Project
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        return head.getElement();
    }


    public Product last() throws EmptyCollectionException {
        // To be completed as a Programming Project
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        return tail.getElement();
    }

    public boolean contains(Product targetElement) throws
            EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        ProductNode<Product> current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                current = current.getNext();
            }

        if (!found)
            return false;
        else
            return true;
    }

    public void add(Product element) {
        ProductNode<Product> node = new ProductNode<Product>(element);

        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
        modCount++;

    }

    public void insert(Product element, int A) {
        ProductNode<Product> node = new ProductNode<Product>(element);
        ProductNode<Product> current = head;

        if (A == 0) {
            node.setNext(current);
            head = node;
        } else {
            for (int i = 1; i < A; i++)
                current = current.getNext();
            node.setNext(current.getNext());
            current.setNext(node);
        }

        count++;
        modCount++;
    }

    public int size() {
        return count;
    }

    public boolean isEmpty() {
        if (size() == 0)
            return true;
        else
            return false;
    }

    public String toString() {
        String result = "";
        int n = count;
        ProductNode<Product> node = head;
        while (n > 0) {
            result += node.getElement() + "\n";
            node = node.getNext();
            n--;
        }
        return result;
    }

    public void seletionSort() {
        Product temp;
        ProductNode current = head;
        while (current != null) {
            ProductNode node = current.getNext();
            while (node != null) {
                if (node.getElement().compareTo(current.getElement()) < 0) {
                    temp = node.getElement();
                    node.setElement(current.getElement());
                    current.setElement(temp);
                }
                node = node.getNext();
            }
            current = current.getNext();
        }

    }
}

