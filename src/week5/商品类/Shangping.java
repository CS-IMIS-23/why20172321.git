package week5.商品类;

public class Shangping {
    public static void main(String[] args) {
        ProductList linkedUnorderedList = new ProductList();
        Product product1 = new Product("苹果一个", 12);
        Product product2 = new Product("烂牛肉一坨", 11);
        Product product3 = new Product("拖鞋一只", 13);
        Product product4 = new Product("烟一盒", 24);
        Product product5 = new Product("java一本", 2000);
        linkedUnorderedList.add(product1);
        linkedUnorderedList.add(product2);
        linkedUnorderedList.add(product3);
        linkedUnorderedList.add(product4);
        System.out.println("加入四种商品" + "\n" + linkedUnorderedList.toString());
        linkedUnorderedList.insert(product5, 3);
        System.out.println("插入Java一本" + "\n" + linkedUnorderedList.toString());
        linkedUnorderedList.seletionSort();
        System.out.println("按价格排序" + "\n" + linkedUnorderedList.toString());
        linkedUnorderedList.removeFirst();
        linkedUnorderedList.removeLast();
        System.out.println("删除最高价和最低价" + "\n" + linkedUnorderedList.toString());
        System.out.println("商品5是否还在" + "\n" + linkedUnorderedList.contains(product5));
        linkedUnorderedList.remove(product5);
        System.out.println("商品5是否还在" + "\n" + linkedUnorderedList.contains(product5));


    }
}
