package week5.商品类;

public class ProductNode<Product extends week5.商品类.Product> {
    private ProductNode<Product> next;
    private Product element;


    public ProductNode() {
        next = null;
        element = null;
    }


    public ProductNode(Product elem) {
        next = null;
        element = elem;
    }

    public ProductNode<Product> getNext() {
        return next;
    }


    public void setNext(ProductNode<Product> node) {
        next = node;
    }


    public week5.商品类.Product getElement() {
        return element;
    }


    public void setElement(Product elem) {
        element = elem;
    }
}