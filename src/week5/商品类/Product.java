package week5.商品类;

import java.io.Serializable;

public class Product implements Serializable, Comparable<Product> {
    private String name;
    private int money;

    public Product(String name, int money) {
        this.name = name;
        this.money = money;
    }

    public String getName() {
        return name;
    }

    public int getMoney() {
        return money;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setMoney(int money) {
        this.money = money;
    }


    @Override
    public String toString() {
        String result = name + " " + money;
        return result;
    }

    @Override
    public int compareTo(Product product) {
        if (money > product.getMoney()) {
            return 1;
        } else {
            if (money < product.getMoney()) {
                return -1;
            } else {
                if (name.compareTo(product.getName()) > 0) {
                    return 1;
                } else {
                    if (name.compareTo(product.getName()) < 0) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
        }
    }


}
