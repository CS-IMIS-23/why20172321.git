package week3;

public class LinkedStackTest {
    public static void main(String[] args) {
        LinkedStack linkedStack = new LinkedStack();

        System.out.println("空栈的元素个数" + linkedStack.size());
        System.out.println("栈是否为空：" + linkedStack.isEmpty());
        linkedStack.push(2);
        linkedStack.push(1);
        linkedStack.push(4);
        System.out.println("栈中所有元素:" + linkedStack.toString());
        System.out.println("显示栈顶元素：" + linkedStack.peek());
        System.out.println("栈的长度：" + linkedStack.size());
        System.out.println("栈是否为空：" + linkedStack.isEmpty());
        linkedStack.pop();
        System.out.println("pop一次后栈中元素：" + linkedStack.toString());
        linkedStack.pop();
        System.out.println("pop两次后的长度" + linkedStack.size());
        linkedStack.pop();
        System.out.println("pop三次后是否为空：" + linkedStack.isEmpty());
    }
}
