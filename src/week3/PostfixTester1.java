package week3;

//pp4.2，修改例题3.1，使用LinkedStack而不是ArrayStack

import java.util.Scanner;

public class PostfixTester1 {
    public static void main(String[] args) {
        String expression, again;
        int result;

        Scanner in = new Scanner(System.in);


        do {
            PostfixEvaluator1 evaluator = new PostfixEvaluator1();
            System.out.println("Entera vaild post-fix expression one token " + " at a time with a space between each token (e.g 5 4 + 3 2 1 - + *)");
            System.out.println("Each token must be an integer or an operator (+,-,*,/)");
            expression = in.nextLine();

            result = evaluator.evaluate(expression);
            System.out.println();
            System.out.println("That expression equals " + result);

            System.out.println("Evaluate another expression [Y/N]?");
            again = in.nextLine();
            System.out.println();
        }
        while (again.equalsIgnoreCase("y"));

    }
}