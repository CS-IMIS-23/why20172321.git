package week4;

import week4.课本代码.CircularArrayQueue;

public class CircularArrayQueueTest {
    public static void main(String[] args) {
        CircularArrayQueue caq = new CircularArrayQueue(2);
        caq.enqueue(2);
        caq.enqueue(3);
        caq.enqueue(2);
        caq.enqueue(1);
        System.out.println(caq.size());
        System.out.println(caq.dequeue());
        System.out.println(caq.toString());
        System.out.println(caq.first());
    }
}
