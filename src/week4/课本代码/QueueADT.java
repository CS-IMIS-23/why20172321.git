package week4.课本代码;

//书上例题5.4，队列ADT

public interface QueueADT<T> {

    public void enqueue(T element);

    public T dequeue();

    public T first();

    public boolean isEmpty();

    public int size();

    public String toString();
}