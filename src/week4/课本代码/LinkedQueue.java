package week4.课本代码;

import week2.EmptyCollectionException;
import week3.LinearNode;

public class LinkedQueue<T> implements QueueADT<T> {
    private int count;
    private LinearNode<T> head, tail;

    public LinkedQueue() {
        count = 0;
        head = tail = null;
    }

    public void enqueue(T element) {
        LinearNode<T> node = new LinearNode<T>(element);

        if (isEmpty())
            head = node;
        else
            tail.setNext(node);
        tail = node;
        count++;
    }

    public T dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = head.getElement();
        head = head.getNext();
        count--;

        if (isEmpty())
            tail = null;

        return result;
    }


    public T first() throws EmptyCollectionException {
//        if (isEmpty())
//            throw new EmptyCollectionException("queue");
        T result = head.getElement();

        return result;
    }

    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        return count;
    }

    public String toString() {
        LinearNode<T> current;
        current = head;
        String result = "";
        while (count != 0) {
            result += current.getElement() + "\t";
            current = current.getNext();
            count--;
        }
        return result;
    }

}
