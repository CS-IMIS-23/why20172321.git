package week4.课本代码;

//书上例题5.2

public class Customer {

    private int arrivalTime, departureTime;

    public Customer(int arrives) {
        this.arrivalTime = arrives;
        this.departureTime = 0;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setDepartureTime(int departureTime) {
        this.departureTime = departureTime;
    }

    public int getDepartureTime() {
        return departureTime;
    }

    public int totalTime() {
        return departureTime - arrivalTime;
    }
}