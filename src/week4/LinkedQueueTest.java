package week4;

import week4.课本代码.LinkedQueue;

public class LinkedQueueTest {
    public static void main(String[] args) {
        LinkedQueue que = new LinkedQueue();
        que.enqueue(2);
        que.enqueue(3);
        que.enqueue(2);
        que.enqueue(1);
        System.out.println(que.size());
        System.out.println(que.dequeue());
        System.out.println(que.toString());
        System.out.println(que.first());
    }
}
