package week4;

import week4.课本代码.CircularArrayQueue;
//使用pp5.2扩展的书上的CircularArrayQueue类

import java.util.Scanner;

public class Pascal {
    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.print("请输入杨辉三角的行数：");
        int n = scan.nextInt();
        CircularArrayQueue<Integer> pascal = new CircularArrayQueue<>();

        //初始值设置为 0,1
        pascal.enqueue(0);
        pascal.enqueue(1);

        int i = 0;
        while (i <= n) {
            int x = pascal.dequeue();
            int y = pascal.first();
            int z = x + y;//得到下一行的数字
            if (x == 0) {
                i++;
                pascal.enqueue(0);
            }
            pascal.enqueue(z);

            //根据每一行的变化输出空字符串达到等腰三角形的效果
            if (x == 0) {
                System.out.println();
                for (int j = 0; j < 2 * (n - i); j++) {
                    System.out.print(" ");
                }
            } else
                System.out.print(x + "   ");
        }
    }
}