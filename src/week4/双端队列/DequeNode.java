package week4.双端队列;

public class DequeNode<T> {
    private DequeNode<T> next, previous;
    private T element;

    public DequeNode() {
        next = null;
        previous = null;
        element = null;
    }

    public DequeNode(T elem) {
        next = null;
        previous = null;
        element = elem;
    }

    public DequeNode<T> getNext() {
        return next;
    }

    public void setNext(DequeNode<T> node) {
        next = node;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T elem) {
        element = elem;
    }


    //比LinearNode多了向前操作的节点

    public DequeNode<T> getPrevious() {
        return previous;
    }

    public void setPrevious(DequeNode<T> node) {
        previous = node;
    }

}