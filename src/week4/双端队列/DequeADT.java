package week4.双端队列;

public interface DequeADT<T> {

    public void enqueue(T element);

    public void Lastenqueue(T element);

    public T dequeue();

    public T Lastdequeue();

    public T first();

    public T last();

    public boolean isEmpty();

    public int size();

    public String toString();
}

