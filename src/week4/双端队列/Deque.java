package week4.双端队列;

import week2.EmptyCollectionException;

public class Deque<T> implements DequeADT<T> {
    private int count;
    private DequeNode<T> top, bottom;

    public Deque() {
        count = 0;
        top = null;
        bottom = null;
    }


    @Override
    public void enqueue(T element) {
        DequeNode<T> node = new DequeNode<>(element);

        if (isEmpty())
            bottom = node;
        else
            top.setPrevious(node);

        node.setNext(top);
        top = node;
        count++;
    }

    @Override
    public void Lastenqueue(T element) {
        DequeNode<T> node = new DequeNode<>(element);

        if (isEmpty())
            top = node;
        else
            bottom.setNext(node);

        node.setPrevious(bottom);
        bottom = node;
        count++;
    }

    @Override
    public T dequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        DequeNode<T> temp = top;
        T result = temp.getElement();
        top = top.getNext();
        count--;

        if (top != null)
            top.setPrevious(null);
        else
            bottom = null;

        return result;

    }

    @Override
    public T Lastdequeue() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        DequeNode<T> temp = bottom;
        T result = temp.getElement();
        bottom = bottom.getPrevious();
        if (bottom != null)
            bottom.setNext(null);
        else
            top = null;

        count--;
        return result;

    }

    @Override
    public T first() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");
        T result = top.getElement();

        return result;
    }

    @Override
    public T last() throws EmptyCollectionException {
        if (isEmpty())
            throw new EmptyCollectionException("queue");
        T result = bottom.getElement();

        return result;
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        return count;
    }

    public String toString() {
        DequeNode<T> current;
        current = top;
        String result = "";
        int i = count;
        while (i != 0) {
            result += current.getElement() + "\t";
            current = current.getNext();
            i--;
        }
        return result;

    }
}