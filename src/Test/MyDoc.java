package Test;

// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}

// byte
class Byte extends Data {
    int value;

    Byte() {
        value = 20172321 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}

//short
class Short extends Data {
    int value;

    Short() {
        value = 20172321 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}

//Boolean
class Boolean extends Data {
    int value;

    Boolean() {
        value = 20172321 % 6;

    }

    public void DisplayValue() {
        System.out.println(value);
    }
}

// long
class Long extends Data {
    int value;

    Long() {
        value = 20172321 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}

//float
class Float extends Data {
    int value;

    Float() {
        value = 20172321 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}

//double
class Double extends Data {
    int value;

    Double() {
        value = 20172321 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}

// pattern classes
abstract class Factory {
    abstract public Data CreateDataObject();
}

class ByteFactory extends Factory {
    public Data CreateDataObject() {
        return new Byte();
    }
}

class ShortFactory extends Factory {
    public Data CreateDataObject() {
        return new Short();
    }
}

class BooleanFactory extends Factory {
    public Data CreateDataObject() {
        return new Boolean();
    }
}

class LongFactory extends Factory {
    public Data CreateDataObject() {
        return new Long();
    }
}

class FloatFactory extends Factory {
    public Data CreateDataObject() {
        return new Float();
    }
}

class DoubleFactory extends Factory {
    public Data CreateDataObject() {
        return new Double();
    }
}

// Client  classes
class Document {
    Data pd;

    Document(Factory pf) {
        pd = pf.CreateDataObject();
    }

    public void DisplayData() {
        pd.DisplayValue();
    }
}

// Test class
public class MyDoc {
    static Document a, b, c, d, e, f;

    public static void main(String[] args) {
        a = new Document(new ByteFactory());
        a.DisplayData();
        b = new Document(new ShortFactory());
        b.DisplayData();
        c = new Document(new BooleanFactory());
        c.DisplayData();
        d = new Document(new LongFactory());
        d.DisplayData();
        e = new Document(new FloatFactory());
        e.DisplayData();
        f = new Document(new DoubleFactory());
        f.DisplayData();
    }
}