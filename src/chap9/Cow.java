package chap9;

public class Cow extends Animal {

    public Cow(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println("我也吃草呢");
    }

    @Override
    public void sleep() {
        System.out.println("我也睡觉呢");
    }

    @Override
    public void introduction() {
        System.out.println("我是 " + id + " " + name);

    }
}