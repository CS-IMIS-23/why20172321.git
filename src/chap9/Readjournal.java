package chap9;

public class Readjournal extends Readingmaterials {
    private String serialnumber;

    public Readjournal(int ps, String be, String kd, String sr) {
        super(ps, be, kd);
        serialnumber = sr;
    }

    public void setSerialnumber(String sr) {
        this.serialnumber = sr;
    }

    public String getSerialnumber() {
        return serialnumber;
    }

    public String toString() {
        String result = " 报纸名称:" + bookname + " 页数:" + pages + " 关键词:" + keyword + " 刊号:" + serialnumber;
        return result;
    }
}
