package chap9;

public class AnimalTest {
    public static void main(String[] args) {
        Sheep sheep = new Sheep("小羊", 20182223);
        Cow cow = new Cow("小牛", 20182222);

        sheep.introduction();
        sheep.eat();
        sheep.sleep();

        cow.introduction();
        cow.eat();
        cow.sleep();
    }

}
