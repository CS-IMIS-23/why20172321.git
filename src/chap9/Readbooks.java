package chap9;

public class Readbooks extends Readingmaterials {
    private int dateofpublication;

    public Readbooks(int ps, String be, String kd, int dn) {
        super(ps, be, kd);
        dateofpublication = dn;
    }

    public void setAuthor(int dn) {
        this.dateofpublication = dn;
    }

    public int getDateofpublication() {
        return dateofpublication;
    }

    public String toString() {
        String result = " 书籍:" + bookname + " 页数:" + pages + " 关键词:" + keyword + " 出版日期:" + dateofpublication;
        return result;
    }
}
