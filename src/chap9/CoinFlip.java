package chap9;

public class CoinFlip {
    public static void main(String[] args) {
        MonetaryCoin myCoin1 = new MonetaryCoin();
        MonetaryCoin myCoin2 = new MonetaryCoin();
        MonetaryCoin myCoin3 = new MonetaryCoin();
        MonetaryCoin myCoin4 = new MonetaryCoin();

        System.out.println("四个硬币的和为" + (myCoin1.getF() + myCoin2.getF() + myCoin3.getF() + myCoin4.getF()));
    }
}
