package chap9;

public class Readreferencebook extends Readingmaterials {
    private String author;

    public Readreferencebook(int ps, String be, String kd, String ar) {
        super(ps, be, kd);
        author = ar;
    }

    public void setAuthor(String ar) {
        this.author = ar;
    }

    public String getAuthor() {
        return author;
    }

    public String toString() {
        String result = " 参考书名称:" + bookname + " 页数:" + pages + " 关键词:" + keyword + " 主编:" + author;
        return result;
    }
}
