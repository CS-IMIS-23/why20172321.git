package chap9;

public class Sheep extends Animal {
    public Sheep(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println("我吃草呢");
    }

    @Override
    public void sleep() {
        System.out.println("我睡觉呢");
    }

    @Override
    public void introduction() {
        System.out.println("我是 " + id + " " + name);
    }
}
