package chap9;

public class Readingmaterials {
    protected int pages;
    protected String bookname;
    protected String keyword;

    public Readingmaterials(int ps, String be, String kd) {
        pages = ps;
        bookname = be;
        keyword = kd;
    }

    //---------------------------------------
    //页数
    //------------------------------------
    public void setPages(int ps) {
        pages = ps;
    }

    public int getPages() {
        return pages;
    }

    //-------------------------------------------
    //名字
    //-----------------------------------------
    public void setBookname(String be) {
        bookname = be;
    }

    public String getBookname() {
        return bookname;
    }

    //----------------------------------------------------------
    //关键词
    //----------------------------------------------------------
    public void setKeyword(String kd) {
        this.keyword = kd;
    }

    public String getKeyword() {
        return keyword;
    }
}
