package chap9;

public class ReadingmaterialsTest {
    public static void main(String[] args) {
        Readnovel a = new Readnovel(405, "小说", "好看", "言情");
        Readjournal b = new Readjournal(34, "报纸", "日报", "ISSN8888");
        Readreferencebook c = new Readreferencebook(500, "五高三模", "难得一匹", "曲一线");
        Readbooks d = new Readbooks(250, "好书", "书", 2018314);

        System.out.println(a + "\n\n" + b + "\n\n" + c + "\n\n" + d);
    }
}
