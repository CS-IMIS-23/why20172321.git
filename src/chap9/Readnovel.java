package chap9;

public class Readnovel extends Readingmaterials {
    private String noveltype;

    public Readnovel(int ps, String be, String kd, String ne) {
        super(ps, be, kd);
        noveltype = ne;
    }

    public void setNoveltype(String ne) {
        this.noveltype = ne;
    }

    public String getNoveltype() {
        return noveltype;
    }

    public String toString() {
        String result = " 书名:" + bookname + " 页数:" + pages + " 关键词:" + keyword + " 小说类型:" + noveltype;
        return result;
    }
}
