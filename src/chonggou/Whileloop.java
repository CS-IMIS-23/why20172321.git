package chonggou;


import java.util.Scanner;

public class Whileloop {
    public static void main(String[] args) {
        int number;
        int nWhile = 1;
        int n = 1;

        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the number: ");
        number = scan.nextInt();
        while (n < number) {
            n++;
            nWhile *= n;
        }
        System.out.println("n! = " + nWhile);

    }
}

