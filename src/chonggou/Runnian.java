package chonggou;


import java.util.Scanner;

//编写程序，读取用户输入的一个整数作为年份，并且确定改年是否为闰年，
// 这样2月有29天，年份能被4而且不能被100整除，或者能被100和400同时
// 整除为闰年。如果输入的年份小于1582则程序错误。
public class Runnian {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("输入年份：");
        int year = scan.nextInt();

        if (year < 1582) {
            System.out.println("1582年以前未采用公历。");
        } else if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
            System.out.println("是闰年。");
        } else {
            System.out.println("不是闰年");
        }


    }


}

