package chonggou;

public class Car {
    public String automonilemanufacturer;
    public String modelnumber;
    public long yearofmanufacturer;

    public Car(String automonile, String number, long year) {
        automonilemanufacturer = automonile;
        modelnumber = number;
        yearofmanufacturer = (long) year;
    }

    public void setAutomonilemanufacturer(String automonile) {
        automonilemanufacturer = automonile;
    }

    public void setModelnumber(String number) {
        modelnumber = number;
    }

    public void Yearofmanufacturer(long year) {
        yearofmanufacturer = year;
    }

    @Override
    public String toString() {

        return automonilemanufacturer + "\t" + modelnumber + "\t" + yearofmanufacturer;
    }

    public String getAutomonilemanufacturer() {
        return automonilemanufacturer;
    }

    public String getModelnumber() {
        return modelnumber;
    }

    public long getYearofmanufacturer() {
        return yearofmanufacturer;
    }


    public boolean isAntique() {
        if (yearofmanufacturer <= 1973) {
            System.out.println("\"ANTIQUE CAR\"");
        }
        return false;
    }


}


