package shiyan1;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

public class shiyanyi1 {
    private NumList list;
    private int nWuhengyi;
    private int pos = 0;// 节点的位置

    public shiyanyi1() {
        nWuhengyi = 0;
        list = null;
    }

    private class NumList {
        public Number Num;
        public NumList next;

        public NumList(Number num) {
            Num = num;
            next = null;
        }
    }


    public void Selection() {
        //对链表中的整数按由小到大排序
        NumList current;
        current = list;
        int[] A = new int[nWuhengyi];
        for (int i = 0; i < nWuhengyi; i++) {
            A[i] = current.Num.number;
            current = current.next;
        }
        Select select = new Select();
        select.sort(A);
    }

    //--------------选择排序法
    public class Select {
        public void sort(int arr[]) {
            int temp = 0;

            for (int j = 0; j < arr.length - 1; j++) {
                int min = arr[j];
                int minIndex = j;
                for (int k = j + 1; k < arr.length; k++) {
                    if (min > arr[k]) {
                        min = arr[k];
                        minIndex = k;
                    }
                }
                temp = arr[j];
                arr[j] = arr[minIndex];
                arr[minIndex] = temp;

                System.out.print("第" + (j + 1) + "次排序后的结果是：");
                for (int h = 0; h < arr.length; h++) {
                    System.out.print(arr[h] + " ");
                }
                System.out.println("元素总数" + size());
            }
            //输出结果
            System.out.print("最后排序结果：");
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + "   ");
            }

        }
    }


    // 插入一个头节点,第二次操作
    public void addFirstNumList(Number data) {
        NumList node = new NumList(data);
        node.next = list;
        list = node;

        nWuhengyi++;
    }

    // 删除一个头结点,并返回头结点
    public NumList deleteFirstNumList() {
        NumList tempNode = list;
        list = tempNode.next;

        nWuhengyi--;
        return tempNode;
    }

    // 在任意位置插入节点 在index的后面插入,第二次操作的修改
    public void insert(int index, Number data) {
        NumList node = new NumList(data);
        NumList current = list;
        NumList previous = list;
        while (pos != index) {
            previous = current;
            current = current.next;
            pos++;
        }
        node.next = current;
        previous.next = node;
        pos = 0;
        nWuhengyi++;
    }

    // 删除任意位置的节点,第二次操作的修改
    public NumList delete(int index) {
        NumList current = list;
        NumList previous = list;
        while (pos != index) {
            pos++;
            previous = current;
            current = current.next;
        }
        if (current == list) {
            list = list.next;
        } else {
            pos = 0;
            previous.next = current.next;
        }
        nWuhengyi--;
        return current;
    }

    // 根据位置输出节点信息,第二次操作
    public NumList findByPos(int index) {
        NumList current = list;
        if (pos != index) {
            current = current.next;
            pos++;
        }
        return current;
    }

    public void add(Number num) {
        //链表添加
        NumList node = new NumList(num);
        NumList current;

        if (list == null) {
            node.next = list;
            list = node;
        } else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }

        nWuhengyi++;
    }


    public String toString() {
        String result = "";

        NumList current = list;

        while (current != null) {
            result += current.Num.number + " ";
            current = current.next;

        }

        return result;
    }

    public int size() {
        return nWuhengyi;
    }

    public static void main(String[] args) throws IOException {
        shiyanyi1 shiyanyi1 = new shiyanyi1();

        System.out.println("请键入所需要的数字： （需要用空格隔开）");
        Scanner scanner = new Scanner(System.in);//把20 18 23 21形式的数字串加入栈
        String shuru = scanner.nextLine();
        StringTokenizer str = new StringTokenizer(shuru);
        for (int a = 0; a < shuru.length(); a++) {
            while (str.hasMoreTokens()) {
                String fanxu = str.nextToken();
                Number num = new Number(Integer.parseInt(fanxu));
                shiyanyi1.add(num);
            }
        }

        System.out.println("栈中所有元素:" + shiyanyi1.toString());
        System.out.println("栈中元素数量:" + shiyanyi1.size());

        //第二次操作，加入文件提取
        File file = new File("C:\\Users\\Administrator", "aaa.txt");
        //
        //使用try-catch处理异常
        //
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException Exception) {
            System.out.println("错误，指定路径不存在");
        }

        InputStream inputStream1 = new FileInputStream(file);
        System.out.print("文件内容：");
        while (inputStream1.available() > 0) {
            System.out.print((char) inputStream1.read() + "  ");
        }
        System.out.println(" ");
        inputStream1.close();

        //System.out.println(" ");
        byte[] buffer = new byte[1024];
        String content = "";
        int flag = 0;
        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);

        while ((flag = bufferedInputStream.read(buffer)) != -1) {
            content += new String(buffer, 0, flag);
        }
        //System.out.println(content);
        bufferedInputStream.close();

        //把数字以int形式提取出来
        String[] a = new String[10];
        a = content.split(" ");

        String B = a[0];
        String C = a[1];

        Number i = new Number(Integer.valueOf(B).intValue());//i = 1
        Number ii = new Number(Integer.valueOf(C).intValue());//ii = 2

        shiyanyi1.insert(5, i);
        System.out.println("栈中所有元素:" + shiyanyi1.toString());
        System.out.println("栈中元素数量:" + shiyanyi1.size());

        shiyanyi1.addFirstNumList(ii);
        System.out.println("栈中所有元素:" + shiyanyi1.toString());
        System.out.println("栈中元素数量:" + shiyanyi1.size());

        shiyanyi1.delete(6);
        System.out.println("栈中所有元素:" + shiyanyi1.toString());
        System.out.println("栈中元素数量:" + shiyanyi1.size());

        shiyanyi1.Selection();
        System.out.println("栈中元素数量:" + shiyanyi1.size());
    }

}