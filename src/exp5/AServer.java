package exp5;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by besti on 2018/6/9.
 */
public class AServer {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket = new ServerSocket(8800);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket = serverSocket.accept();
        //3.获得输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        //获得输出流
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info = null;
        String str = "";
        while (!((info = bufferedReader.readLine()) == null)) {
            System.out.println("密钥编码的内容：" + info);
            str = info;
        }
        String[] a = str.split(",");
        byte[] ab = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            ab[i] = Byte.parseByte(a[i]);
        }
        // 获取密钥
        FileInputStream f3 = new FileInputStream("keykb1.dat");
        int num2 = f3.available();
        byte[] keykb = new byte[num2];
        f3.read(keykb);
        SecretKeySpec k = new SecretKeySpec(keykb, "DESede");
        // 解密
        Cipher cp = Cipher.getInstance("DESede");
        cp.init(Cipher.DECRYPT_MODE, k);
        byte[] ptext = cp.doFinal(ab);
        // 显示明文
        String p = new String(ptext, "UTF8");
        System.out.println("解密的后缀表达式：" + p);
        MyDC evaluator = new MyDC();
        int result = evaluator.evaluate(p);
        String reply = String.valueOf(result);

        //给客户一个响应
        printWriter.write(reply);
        printWriter.flush();
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}