package exp5;


import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by besti on 2018/6/9.
 */
public class SocketServer {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket = new ServerSocket(8800);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket = serverSocket.accept();
        //3.获得输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        //获得输出流
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        //4.读取用户输入信息
//

        String info = null;
        int result = 0;
        while (!((info = bufferedReader.readLine()) == null)) {
            System.out.println("接收到的： " + info);


            String expressions;
            MyDC evaluator = new MyDC();
            expressions = info;
            result = evaluator.evaluate(expressions);//
            System.out.println("计算结果为：" + result);

        }

        //给客户一个响应
        String reply = String.valueOf(result);
        printWriter.write(reply);
        printWriter.flush();
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}
