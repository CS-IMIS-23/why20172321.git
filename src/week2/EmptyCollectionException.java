package week2;

//特点的空栈异常

public class EmptyCollectionException extends RuntimeException {

    public EmptyCollectionException(String collection) {
        super("The " + collection + " is empty");
    }
}
