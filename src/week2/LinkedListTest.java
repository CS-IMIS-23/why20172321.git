package week2;

import java.util.Scanner;
import java.util.StringTokenizer;

public class LinkedListTest {
    public static void main(String[] args) {
        LinkedListNumber Num = new LinkedListNumber();
        System.out.println("请输入整数");
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        StringTokenizer a = new StringTokenizer(str);
        while (a.hasMoreTokens()) {
            Num.add(new Number(Integer.parseInt(a.nextToken())));
        }
        Num.Selection();
        System.out.println("排序后：" + Num.toString());

        System.out.println(" ");
        System.out.println("以下是对节点插入、删除、输出操作的测试");
        LinkedListNumber N = new LinkedListNumber();
        Number num1 = new Number(13);
        Number num2 = new Number(11);
        Number num3 = new Number(21);
        Number num4 = new Number(15);
        Number num5 = new Number(24);
        N.add(num1);
        N.add(num2);
        N.add(num3);
        N.add(num4);
        N.add(num5);
        System.out.println("链表中加入五个节点  " + N.toString());
        N.delete(num2);
        System.out.println("删除num2  " + N.toString());
        N.insert(num3, num5);
        System.out.println("把num2插入到num5前  " + N.toString());
        N.Selection();
        System.out.println("从小到大排序  " + N.toString());
    }
}