package week2;

//例题3.3，泛型数据集合接口

public interface StackADT<T> {
    public void push(T element);

    public T pop();

    public T peek();

    public boolean isEmpty();

    public int size();

    public String toString();

}
