package week2;

//对输入的句子中每个单词颠倒

import java.util.Scanner;

public class Diandao {
    public static void main(String[] args) throws Exception {
        Stack stack = new Stack(1000);

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String str = scanner.next();

            for (int i = 0; i < str.length(); i++) {
                stack.push(str.charAt(i));
            }
            while (stack.top >= 0) {
                System.out.print(stack.pop());
            }
            System.out.print(" ");
        }

    }
}
