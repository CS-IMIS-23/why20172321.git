package week2;

public class LinkedListNumber {
    private NumList list;
    private int top = 0;

    public LinkedListNumber() {
        list = null;
    }

    private class NumList {

        public Number Num;
        public NumList next;

        public NumList(Number num) {

            Num = num;
            next = null;
        }
    }

    public void add(Number num) {
        //链表添加
        NumList node = new NumList(num);
        NumList current;

        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }

        top += 1;
    }

    public void delete(Number num) {
        //链表删除
        NumList node = new NumList(num);
        NumList current, temp;

        if (list.Num.number == num.number) {
            current = list.next;
            list = current;
        } else {
            current = list;
            while (current.next.Num.number != num.number)
                current = current.next;
            temp = current.next.next;
            current.next = temp;
        }

        top -= 1;
    }

    public void insert(Number num1, Number num2) {
        //链表插入，在num2前插入num1
        NumList node = new NumList(num1);
        NumList current;

        if (list.Num.number == num2.number) {
            current = list;
            node.next = current;
            current.next = node;
        } else {
            current = list;
            while (current.next.Num.number != num2.number)
                current = current.next;

            node.next = current.next;
            current.next = node;
        }

        top += 1;
    }

    public void Selection() {
        //对链表中的整数按由小到大排序
        NumList current;
        current = list;
        int[] A = new int[top];
        for (int i = 0; i < top; i++) {
            A[i] = current.Num.number;
            current = current.next;
        }

        Sort sort = new Sort();
        int[] B = sort.selectionSort(A);

        list = null;
        int top2 = top;
        for (int i = 0; i < top2; i++) {
            Number num = new Number(B[i]);
            add(num);
        }
    }

    public String toString() {
        String result = "";

        NumList current = list;

        while (current != null) {
            result += current.Num.number + " ";
            current = current.next;

        }

        return result;
    }

    public class Sort {
        //选择排序法
        public int[] selectionSort(int[] list) {
            int min;
            int temp;

            for (int index = 0; index < list.length - 1; index++) {
                min = index;
                for (int scan = index + 1; scan < list.length; scan++)
                    if (list[scan] - (list[min]) < 0)
                        min = scan;

                temp = list[min];
                list[min] = list[index];
                list[index] = temp;
            }
            return list;
        }
    }


}