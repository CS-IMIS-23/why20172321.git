package week2;

//输出输入句子的反序

import java.util.Scanner;
import java.util.StringTokenizer;

public class reverse {
    public static void main(String[] args) {

        String shuru, fanxu;

        //输入句子
        Scanner scanner = new Scanner(System.in);
        System.out.println("输入句子");
        shuru = scanner.nextLine();

        //将句子中以空格隔开的内容分别入栈
        StringTokenizer str = new StringTokenizer(shuru);
        ArrayStack stack = new ArrayStack();

        for (int a = 0; a < shuru.length(); a++) {
            while (str.hasMoreTokens()) {
                fanxu = str.nextToken();
                stack.push(fanxu);
            }
        }

        //出栈得到反序
        int c = stack.size();
        System.out.println("得到反序");
        for (int b = 0; b < c; b++) {
            System.out.print(stack.pop() + " ");
        }

    }
}
