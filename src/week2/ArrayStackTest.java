package week2;

public class ArrayStackTest {
    public static void main(String[] args) {
        ArrayStack arrayStack = new ArrayStack();
        System.out.println("空栈的元素个数" + arrayStack.size());
        System.out.println("栈是否为空：" + arrayStack.isEmpty());

        arrayStack.push(2);
        arrayStack.push(1);
        arrayStack.push(4);
        System.out.println("栈中所有元素:" + arrayStack.toString());
        System.out.println("显示栈顶元素：" + arrayStack.peek());
        System.out.println("栈的长度：" + arrayStack.size());
        System.out.println("栈是否为空：" + arrayStack.isEmpty());
        arrayStack.pop();
        System.out.println("pop一次后栈中元素：" + arrayStack.toString());
        arrayStack.pop();
        System.out.println("pop两次后的长度" + arrayStack.size());
        arrayStack.pop();
        System.out.println("pop三次后是否为空：" + arrayStack.isEmpty());
        System.out.println("异常测试");
        arrayStack.pop();
    }
}
