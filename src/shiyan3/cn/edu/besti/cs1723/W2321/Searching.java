package shiyan3.cn.edu.besti.cs1723.W2321;

public class Searching {
    public static <T>
    boolean linearSearch(T[] data, int min, int max, T target) {
        int index = min;
        boolean found = false;

        while (!found && index <= max) {
            found = data[index].equals(target);
            index++;
        }

        return found;
    }
}
