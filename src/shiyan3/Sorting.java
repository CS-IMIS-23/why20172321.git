package shiyan3;

import week8.书上代码.LinkedBinarySearchTree1;
import week9.课本代码.jsjf.LinkedHeap;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;

public class Sorting {
    public static <T extends Comparable<T>>
    T selectionSort(T[] data) {
        int min;
        T temp;

        for (int index = 0; index < data.length - 1; index++) {
            min = index;
            for (int scan = index + 1; scan < data.length; scan++)
                if (data[scan].compareTo(data[min]) < 0)
                    min = scan;

            swap(data, min, index);
        }
        return (T) Arrays.toString(data);
    }

    private static <T extends Comparable<T>>
    void swap(T[] data, int index1, int index2) {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }


    //希尔排序
    public void shellSort(Comparable[] data) {
        int d = data.length;
        while (d != 1) {
            d = d / 2;
            for (int i = 0; i < d; i++) {
                for (int j = i + d; j < data.length; j = j + d) {
                    Comparable temp = data[j];
                    int x;
                    for (x = j - d; x >= 0 && data[x].compareTo(temp) > 0; x = x - d)
                        data[x + d] = data[x];
                    data[x + d] = temp;
                }
            }
        }
    }


    //二叉树排序
    public void binaryTreeSort(Comparable[] data) {
        LinkedBinarySearchTree1 linkedBinarySearchTree = new LinkedBinarySearchTree1();
        for (int i = 0; i < data.length; i++)
            linkedBinarySearchTree.addElement(data[i]);
        for (int i = 0; i < data.length; i++) {
            data[i] = (Comparable) linkedBinarySearchTree.findMin();
            linkedBinarySearchTree.removeMin();
        }
    }


    //堆排序
    public void heapSort(Comparable[] data) {
        Comparable[] result = data;
        LinkedHeap linkedMaxHeap = new LinkedHeap();
        for (int i = 0; i < result.length; i++)
            linkedMaxHeap.addElement(result[i]);
        for (int i = result.length - 1; i >= 0; i--)
            data[i] = (Comparable) linkedMaxHeap.removeMin();
    }


    //桶排序
    public static void bucketSort(int[] arr) {
        int bucketCount = 10;
        Integer[][] bucket = new Integer[bucketCount][arr.length];
        for (int i = 0; i < arr.length; i++) {
            int quotient = arr[i] / 10;
            for (int j = 0; j < arr.length; j++) {
                if (bucket[quotient][j] == null) {
                    bucket[quotient][j] = arr[i];
                    break;
                }
            }
        }
        //小桶排序
        for (int i = 0; i < bucket.length; i++) {
            for (int j = 1; j < bucket[i].length; ++j) {
                if (bucket[i][j] == null) {
                    break;
                }
                int value = bucket[i][j];
                int position = j;
                while (position > 0 && bucket[i][position - 1] > value) {
                    bucket[i][position] = bucket[i][position - 1];
                    position--;
                }
                bucket[i][position] = value;
            }

        }
        //输出
        for (int i = 0, index = 0; i < bucket.length; i++) {
            for (int j = 0; j < bucket[i].length; j++) {
                if (bucket[i][j] != null) {
                    arr[index] = bucket[i][j];
                    index++;
                } else {
                    break;
                }
            }
        }
    }
}
