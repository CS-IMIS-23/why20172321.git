package shiyan3;

import week8.书上代码.LinkedBinarySearchTree;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

public class Searching {
    //线性查找
    public static <T>
    boolean linearSearch(T[] data, int min, int max, T target) {
        int index = min;
        boolean found = false;

        while (!found && index <= max) {
            found = data[index].equals(target);
            index++;
        }

        return found;
    }

    //折半查找
    public static <T extends Comparable<T>>
    boolean binarySearch(T[] data, int min, int max, T target) {
        boolean found = false;
        int midpoint = (min + max) / 2;  // determine the midpoint

        if (data[midpoint].compareTo(target) == 0)
            found = true;

        else if (data[midpoint].compareTo(target) > 0) {
            if (min <= midpoint - 1)
                found = binarySearch(data, min, midpoint - 1, target);
        } else if (midpoint + 1 <= max)
            found = binarySearch(data, midpoint + 1, max, target);

        return found;
    }

    //插值查找
    public static int InsertionSearch(int[] arr, int min, int max, int key) {
        int low = 1;
        int high = max - min;
        while (low <= high) {
            int mid = low + (high - low) * (key - arr[low]) / (arr[high] - arr[low]);/*插值*/
            if (arr[mid] < key)
                low = mid + 1;   //要+1
            else if (arr[mid] > key)
                high = mid - 1;  //要-1
            else
                return mid;
        }
        return 0;
    }

    //斐波那契查找
    public static int FibonacciSearch(int[] arr, int min, int max, int key) {
        int n = max - min;
        int low = 1;  //记录从1开始
        int high = n;
        int mid;

        int k = 0;
        while (n > Fibonacci(k) - 1)    //获取k值
            k++;
        int[] temp = new int[Fibonacci(k)];   //因为无法直接对原数组arr[]增加长度，所以定义一个新的数组
        System.arraycopy(arr, 0, temp, 0, arr.length); //采用System.arraycopy()进行数组间的赋值
        for (int i = n + 1; i <= Fibonacci(k) - 1; i++)    //对数组中新增的位置进行赋值
            temp[i] = temp[n];

        while (low <= high) {
            mid = low + Fibonacci(k - 1) - 1;
            if (temp[mid] > key) {
                high = mid - 1;
                k = k - 1;  //对应上图中的左段，长度F[k-1]-1
            } else if (temp[mid] < key) {
                low = mid + 1;
                k = k - 2;  //对应上图中的右端，长度F[k-2]-1
            } else {
                if (mid <= n)
                    return mid;
                else
                    return n;       //当mid位于新增的数组中时，返回n
            }
        }
        return 0;
    }

    /*
     * 斐波那契数列
     * 不采用递归
     */
    public static int Fibonacci(int n) {
        int a = 0;
        int b = 1;
        if (n == 0)
            return a;
        if (n == 1)
            return b;
        int c = 0;
        for (int i = 2; i <= n; i++) {
            c = a + b;
            a = b;
            b = c;
        }
        return c;
    }

    //数表查找，利用二叉查找树实现
    public static Comparable treeSearch(int[] data, int target) {
        LinkedBinarySearchTree<Integer> Tree = new LinkedBinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            Tree.addElement(data[i]);
        }

        return Tree.contains(target);
    }


    // 分块查找

    public static int blockSearch(int[] data, int target, int n) {
        HashMap<Integer, Integer> block = new HashMap<>();
        int tem = data[0];
        for (int i = 0; i < data.length - 1; i++) {
            if (i % n == 0) { //起始分块
                if ((i + n) >= data.length - 1) {
                    for (int j = i; j < data.length; j++) {
                        if (data[j] > tem) {
                            tem = data[j];
                        }
                    }
                    //保存区块极值和起始下标
                    block.put(tem, i);
                } else {
                    for (int j = i; j < i + n; j++) { //区块内查找极值
                        if (data[j] > tem) {
                            tem = data[j];
                        }
                    }
                    block.put(tem, i);
                    tem = data[i + n - 1];
                }
            }
        }

        //获取索引极值进行排序
        Iterator<Integer> ite = block.keySet().iterator();
        int[] index1 = new int[block.size()];
        int i = 0;
        while (ite.hasNext()) {
            index1[i++] = ite.next();
        }
        Arrays.sort(index1);
        for (int j = 0; j < index1.length; j++) {
            if (target <= index1[j]) {
                int start = block.get(index1[j]);
                int end = 0;
                if (j != index1.length - 1) {
                    end = block.get(index1[j + 1]);
                } else {
                    end = data.length;
                }
                //查找区块元素位置
                for (int k = start; k < end; k++) {
                    if (target == data[k]) {
                        return data[k];
                    }
                }
            }
        }
        return -1;//找不到返回-1
    }

    //哈希查找
    public static int hashSearch(int data[], int target) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for (int i = 0; i < data.length; i++)
            hashMap.put(Integer.hashCode(data[i]), data[i]);

        int key = Integer.hashCode(target);
        if (hashMap.containsKey(key))
            return hashMap.get(key);

        return -1;//找不到返回-1
    }
}
