package shiyan3.shiyan3Test;

import junit.framework.TestCase;
import shiyan3.Searching;

/**
 * Searching Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>11/19/2018</pre>
 */
public class SearchingTest extends TestCase {
    public SearchingTest(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Method: linearSearch(T[] data, int min, int max, T target)
     */
    Integer[] list = {1, 2, 3, 4, 5, 6, 7, 8, 23, 21};//正序
    Integer[] list2 = {21, 23, 8, 7, 6, 5, 4, 3, 2, 1};//逆序
    String[] list1 = {"23", "21", "w", "h", "y"};

    public void testLinearSearch1() throws Exception {
//TODO: Test goes here...正常
        assertEquals(true, Searching.linearSearch(list, 0, 9, 2));
        assertEquals(true, Searching.linearSearch(list, 0, 9, 21));
        assertEquals(true, Searching.linearSearch(list1, 0, 4, "21"));
        assertEquals(true, Searching.linearSearch(list1, 0, 4, "w"));
        assertEquals(true, Searching.linearSearch(list2, 0, 9, 2));
    }

    public void testLinearSearch2() throws Exception {
//TODO: Test goes here...异常
        assertEquals(false, Searching.linearSearch(list, 0, 9, 22));
        assertEquals(false, Searching.linearSearch(list, 0, 9, 9));
        assertEquals(false, Searching.linearSearch(list1, 0, 4, 22));
        assertEquals(false, Searching.linearSearch(list1, 0, 4, "e"));
        assertEquals(false, Searching.linearSearch(list2, 0, 9, 9));
    }

    public void testLinearSearch3() throws Exception {
//TODO: Test goes here...边界
        assertEquals(true, Searching.linearSearch(list, 0, 9, 2));
        assertEquals(false, Searching.linearSearch(list, 0, 7, 23));
        assertEquals(false, Searching.linearSearch(list, 1, 7, 1));
        assertEquals(true, Searching.linearSearch(list2, 0, 7, 23));
    }


} 
