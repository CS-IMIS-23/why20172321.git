package shiyan3.shiyan3Test;

import junit.framework.TestCase;
import shiyan3.Sorting;

import static org.junit.Assert.assertNotEquals;

/**
 * Sorting Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>11/19/2018</pre>
 */
public class SortingTest extends TestCase {
    public SortingTest(String name) {
        super(name);
    }

    public void setUp() throws Exception {
        super.setUp();
    }

    public void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Method: selectionSort(T[] data)
     */
    public void testSelectionSort1() throws Exception {
//TODO: Test goes here...正序
        Comparable[] A1 = {1, 2, 3, 4, 5, 6, 7, 8, 23, 21};
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 21, 23]", Sorting.selectionSort(A1));
        Comparable[] A2 = {23, 21, 1, 2, 3, 4, 5, 6, 7, 8};
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 21, 23]", Sorting.selectionSort(A2));
    }

    public void testSortingSearch2() throws Exception {
        //TODO: Test goes here...逆序
        Comparable[] B1 = {21, 23, 8, 7, 6, 5, 4, 3, 2, 1};
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 21, 23]", Sorting.selectionSort(B1));
        Comparable[] B2 = {8, 7, 6, 5, 4, 3, 2, 1, 21, 23};
        assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 21, 23]", Sorting.selectionSort(B2));

    }

    public void testSortingSearch3() throws Exception {
        //TODO: Test goes here...异常
        Comparable[] C1 = {"a", "b", "d", "c", "a"};
        assertNotEquals("[a, b, c, d]", Sorting.selectionSort(C1));
        Comparable[] C2 = {"a", "b", "d", "c", "a"};
        assertNotEquals("[a, b, c, d, 1]", Sorting.selectionSort(C2));

    }

    public void testSortingSearch4() throws Exception {
        //TODO: Test goes here...边界
        Comparable[] D1 = {-99999999, 99999999, 0};
        assertEquals("[-99999999, 0, 99999999]", Sorting.selectionSort(D1));
        Comparable[] D2 = {"a", "z", "g"};
        assertEquals("[a, g, z]", Sorting.selectionSort(D2));

    }
} 
