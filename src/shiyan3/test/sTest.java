package shiyan3.test;

import shiyan3.cn.edu.besti.cs1723.W2321.Searching;
import shiyan3.cn.edu.besti.cs1723.W2321.Sorting;

public class sTest {
    public static void main(String[] args) {

        Integer[] list = {23, 21, 2018, 10, 17, 16, 36};
        System.out.println("原数组为：");
        for (int num : list)
            System.out.print(num + " ");
        System.out.println(" ");
        Sorting.selectionSort(list);
        System.out.println("排序之后为：");
        for (int num : list)
            System.out.print(num + " ");
        System.out.println(" ");
        System.out.println("可以找到21：" + Searching.linearSearch(list, 0, list.length, 21));

    }
}
