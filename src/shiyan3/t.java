package shiyan3;

//结点三

public class t {
    public static void main(String[] args) {
        Searching searching = new Searching();
        Sorting sorting = new Sorting();

        Integer[] list = {23, 21, 2018, 10, 17, 16, 36};
        System.out.println("原数组为：");
        for (int num : list)
            System.out.print(num + " ");
        System.out.println(" ");
        sorting.selectionSort(list);
        System.out.println("排序之后为：");
        for (int num : list)
            System.out.print(num + " ");
        System.out.println(" ");
        System.out.println("顺序查找到21：" + searching.linearSearch(list, 0, list.length, 21));
        System.out.println("二分查找到21：" + searching.binarySearch(list, 0, list.length, 21));

        int[] arr = {23, 21, 2018, 10, 17, 16, 36};
        System.out.println("插值查找21的位置：" + searching.InsertionSearch(arr, 0, arr.length - 1, 21));
        System.out.println("斐波那契查找16的位置：" + searching.FibonacciSearch(arr, 0, arr.length - 1, 16));
        System.out.println("树表查找到21：" + searching.treeSearch(arr, 21));
        System.out.println("分块查找35是否存在：" + searching.blockSearch(arr, 35, 3));
        System.out.println("哈希查找47是否存在：" + searching.hashSearch(arr, 47));


        Integer[] data = {20, 18, 11, 21, 2, 0, 1, 7, 2, 3, 2, 1};
        System.out.println("希尔排序");
        sorting.shellSort(data);
        for (int i = 0; i < data.length; i++)
            System.out.print(data[i] + " ");
        System.out.println();
        System.out.println("堆排序");
        sorting.heapSort(data);
        for (int i = 0; i < data.length; i++)
            System.out.print(data[i] + " ");
        System.out.println();
        System.out.println("二叉树排序");
        sorting.binaryTreeSort(data);
        for (int i = 0; i < data.length; i++)
            System.out.print(data[i] + " ");
        System.out.println();
        int[] data1 = {20, 18, 11, 21, 2, 0, 1, 7, 2, 3, 2, 1};
        System.out.println("桶排序");
        sorting.bucketSort(data1);
        for (int i = 0; i < data1.length; i++)
            System.out.print(data1[i] + " ");
        System.out.println();
        System.out.println("边界测试");
        Integer[] array1 = {-999999, 999999, 0};
        sorting.heapSort(array1);
        for (int i = 0; i < array1.length; i++)
            System.out.print(array1[i] + " ");
        System.out.println();
        System.out.println("异常测试：");
        Integer[] array = {20, 18, 11, 21, 2, 0, 1, 7, 2, 3, 2, 1, Integer.parseInt(null)};
        sorting.selectionSort(array);
        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");

    }
}
