package week8.书上代码;

public class pp113test {
    public static void main(String[] args) {
        LinkedBinarySearchTree linkedBinarySearchTree = new LinkedBinarySearchTree();
        linkedBinarySearchTree.addElement(4);
        linkedBinarySearchTree.addElement(9);
        linkedBinarySearchTree.addElement(3);
        linkedBinarySearchTree.addElement(5);

        System.out.println("二叉查找树为：");
        System.out.println(linkedBinarySearchTree.toString());
        System.out.println("添加1后树为：");
        linkedBinarySearchTree.addElement(1);
        System.out.println(linkedBinarySearchTree.toString());
        System.out.println("树中最大的元素：" + linkedBinarySearchTree.findMax());
        System.out.println("树中最小的元素：" + linkedBinarySearchTree.findMin());
        linkedBinarySearchTree.removeMax();
        System.out.println("删除最大数后树为：");
        System.out.println(linkedBinarySearchTree.toString());
    }
}
