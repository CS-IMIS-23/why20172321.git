package week8;

public class AVLNode<T extends Comparable<T>> {
    public T val;
    public int height;
    public AVLNode<T> left, right;

    public AVLNode(T data) {
        this(null, null, data);
    }

    public AVLNode(AVLNode<T> left, AVLNode<T> right, T data) {
        this(left, right, data, 1);
    }

    public AVLNode(AVLNode<T> left, AVLNode<T> right, T data, int height) {
        this.left = left;
        this.right = right;
        this.val = data;
        this.height = height;
    }
}
