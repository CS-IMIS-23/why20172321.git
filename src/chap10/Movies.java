package chap10;

//Movies.java    Author:Lewis/Loftus
//Demonstrates the use of an array of object.
public class Movies {
    //Creates a DVDCollection object and adds some DVDs to it,Prints
    //reports on the status of the collection.
    public static void main(String[] args) {
        DVDcollection movies = new DVDcollection();

        movies.addDVD("The Godfather", "Francis Ford Coppola", 1972, 24.92, true);
        movies.addDVD("District 9", "Neill Blomkamp", 2009, 19.95, false);
        movies.addDVD("Iron", "Jon Favreau", 2008, 15.95, false);
        movies.addDVD("All About Eve", "Joseph Mankiewicz", 1950, 17.50, false);
        movies.addDVD("The Matrix", "Andy & Lana Wachowski", 1999, 19.95, true);
        System.out.println(movies);

        movies.addDVD("Iron Man 2", "Jon Favreau", 2010, 22.99, false);
        movies.addDVD("Casablanca", "Michael Curtiz", 1942, 19.92, false);

        System.out.println(movies);

        System.out.println("按书名排序");

        DVD[] fri = new DVD[7];

        fri[0] = new DVD("The Godfather", "Francis Ford Coppola", 1972, 24.92, true);
        fri[1] = new DVD("District 9", "Neill Blomkamp", 2009, 19.95, false);
        fri[2] = new DVD("Iron", "Jon Favreau", 2008, 15.95, false);
        fri[3] = new DVD("All About Eve", "Joseph Mankiewicz", 1950, 17.50, false);
        fri[4] = new DVD("The Matrix", "Andy & Lana Wachowski", 1999, 19.95, true);
        fri[5] = new DVD("Iron Man 2", "Jon Favreau", 2010, 22.99, false);
        fri[6] = new DVD("Casablanca", "Michael Curtiz", 1942, 19.92, false);

        DVDcollection.selectionSort(fri);

        for (DVD fris : fri)
            System.out.println(fris);
    }
}