package chap10;

//DVD.java    Author:Lewis/Loftus
//Represents a DVD video disc.

import java.text.NumberFormat;

public class DVD implements Comparable {
    private String title, director;
    private int year;
    private double cost;
    private boolean bluray;

    //Creats a new DVD with the specified information.
    public DVD(String title, String director, int year, double cost, boolean bluray) {
        this.title = title;
        this.director = director;
        this.year = year;
        this.cost = cost;
        this.bluray = bluray;
    }
    //Returns a string description of this DVD.

    public String toString() {
        NumberFormat fmt = NumberFormat.getCurrencyInstance();
        String description;

        description = fmt.format(cost) + "\t" + year + "\t";
        description += title + "\t" + director;
        if (bluray)
            description += "\t" + "Blu-ray";

        return description;
    }

    public boolean equals(Object other) {
        return (title.equals(((DVD) other).getTitle()) &&
                director.equals(((DVD) other).getDirector()));
    }

    public int compareTo(Object other) {
        int result;

        String otherTitle = ((DVD) other).getTitle();
        String otherDirector = ((DVD) other).getDirector();
        if (title.equals(otherTitle))
            result = director.compareTo(otherDirector);
        else
            result = title.compareTo(otherTitle);
        return result;
    }

    public String getDirector() {
        return director;
    }

    public String getTitle() {
        return title;
    }
}
