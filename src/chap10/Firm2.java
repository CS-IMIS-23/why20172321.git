package chap10;

public class Firm2 {
    //-----------------------------------------------------------------
    //  Creates a staff of employees for a firm and pays them.
    //-----------------------------------------------------------------
    public static void main(String[] args) {
        Payable personnel = new Staff2();

        personnel.payday();
    }
}
