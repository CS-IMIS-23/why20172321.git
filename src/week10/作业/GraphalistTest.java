package week10.作业;

public class GraphalistTest {
    public static void main(String[] args) {
        Graphalist graph = new Graphalist();
        graph.addVertex(12);
        graph.addVertex(10);
        graph.addVertex(23);
        graph.addVertex(14);

        graph.addEdge(12, 10);
        graph.addEdge(10, 23);
        graph.addEdge(12, 14);
        graph.addEdge(10, 14);
        System.out.println(graph.toString());
    }
}
