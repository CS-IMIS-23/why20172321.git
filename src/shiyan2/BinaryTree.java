package shiyan2;

import week5.书上代码.jsjf.UnorderedListADT;
import week7.jiumingdaima.ArrayUnorderedList;

import java.util.ArrayList;

public class BinaryTree {
    public BinaryNode root = new BinaryNode(null);

    public BinaryNode getRoot() {
        return root;
    }

    //构建二叉树
    public void build(String str) {
        ArrayList<BinaryNode> numbers = new ArrayList<>();
        ArrayList<BinaryNode> operations = new ArrayList<>();
        BinaryNode binaryNode;
        String snum = "";


        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch >= '0' && ch <= '9') {
                snum += ch + "";
            } else {
                numbers.add(new BinaryNode(snum));
                operations.add(new BinaryNode(ch + " "));
                snum = "";
            }
        }
        numbers.add(new BinaryNode(snum));


        while (!operations.isEmpty()) {
            binaryNode = operations.get(0);
            operations.remove(0);
            binaryNode.setLeft(numbers.get(0));
            binaryNode.setRight(numbers.get(1));
            numbers.remove(0);
            numbers.remove(0);
            root = binaryNode;
            numbers.add(0, binaryNode);
        }
    }

    // 用后序遍历输出表达式二叉树
    public void posorder() {
        System.out.println("后缀表达式为： ");
        posOrder(root);
        System.out.println("");
    }

    public void posOrder(BinaryNode node) {
        if (node != null) {
            posOrder(node.getLeft());
            posOrder(node.getRight());
            System.out.print(node.getData() + " ");
        }
    }


    public int getHeight() {
        // To be completed as a Programming Project
        return height(root);
    }

    private int height(BinaryNode node) {
        if (node == null) {
            return 0;
        } else {
            int i = height(node.getLeft());
            int j = height(node.getRight());
            return (i < j) ? (j + 1) : (i + 1);
        }
    }

    //输出树形
    public String toString() {
        UnorderedListADT<BinaryNode> nodes = new ArrayUnorderedList<BinaryNode>();
        UnorderedListADT<Integer> levelList = new ArrayUnorderedList<Integer>();
        BinaryNode current;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            } else {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {
                result = result + (current.getData()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }

        }

        return result;
    }

}
