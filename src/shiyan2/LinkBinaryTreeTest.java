package shiyan2;

import week7.jiumingdaima.ArrayUnorderedList;
import week7.jiumingdaima.BinaryTreeNode;

public class LinkBinaryTreeTest {
    public static void main(String[] args) {
        BinaryTreeNode n1 = new BinaryTreeNode(1);
        BinaryTreeNode n2 = new BinaryTreeNode(2);
        BinaryTreeNode n3 = new BinaryTreeNode(3);
        BinaryTreeNode n4 = new BinaryTreeNode(4);
        BinaryTreeNode n5 = new BinaryTreeNode(5);
        BinaryTreeNode n6 = new BinaryTreeNode(6);

        LinkedBinaryTree a = new LinkedBinaryTree(n1.getElement());
        LinkedBinaryTree b = new LinkedBinaryTree(n2.getElement());
        LinkedBinaryTree c = new LinkedBinaryTree(n3.getElement());
        LinkedBinaryTree d = new LinkedBinaryTree(n4.getElement(), b, a);
        LinkedBinaryTree e = new LinkedBinaryTree(n5.getElement(), c, d);
        LinkedBinaryTree f = new LinkedBinaryTree(n6.getElement());
        LinkedBinaryTree g = new LinkedBinaryTree(9, f, e);

        System.out.println(g.toString());
        System.out.println("4的right：" + d.getRight());
        System.out.println("是否存在6：" + g.contains(6));
        System.out.println("preorder:" + g.preorder());
        System.out.println("postorder" + g.postorder());
    }
}
