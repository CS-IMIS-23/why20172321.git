package shiyan2;

import WHY.newba.Jisuan;

import java.util.Scanner;

public class Manage {
    public static void main(String[] args) {
        System.out.println("请输入中缀表达式，例如： 55+3*46/2-3");
        Scanner scanner = new Scanner(System.in);
        String shuru = scanner.nextLine();

        BinaryTree binaryTree = new BinaryTree();
        binaryTree.build(shuru);
        System.out.println("二叉树的root为: " + binaryTree.getRoot().getData());

        System.out.println(binaryTree.toString());
        binaryTree.posorder();

        Jisuan evaluator = new Jisuan();
        int result = evaluator.evaluate(shuru);
        System.out.println("计算结果： " + result);
    }
}

