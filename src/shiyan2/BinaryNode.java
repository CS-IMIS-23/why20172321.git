package shiyan2;

public class BinaryNode {
    private String data;
    private BinaryNode left;
    private BinaryNode right;

    public BinaryNode(String data) {
        this.data = data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public BinaryNode getRight() {
        return right;
    }

    public void setRight(BinaryNode node) {
        right = node;
    }

    public BinaryNode getLeft() {
        return left;
    }

    public void setLeft(BinaryNode node) {
        left = node;
    }

}
