package shiyan2;

import java.util.Scanner;

public class getTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree1 tree = new LinkedBinaryTree1();
        String[] preOrder = {"A", "B", "D", "H", "I", "E", "J", "M", "N", "C", "F", "G", "K", "L"};
        String[] inOrder = {"H", "D", "I", "B", "E", "M", "J", "N", "A", "F", "C", "K", "G", "L"};
        tree.getAtree(preOrder, inOrder);
        System.out.println(tree.toString());

    }
}
