//************************
// RollingDice2.java        Author:why
//      
//*********************8

public class RollingDice2
{
//------------------------
// Creates two Die objects and rolls them several times.
// (maybe)
//-----------------------------------
public static void main(String[] args)
{
        PairOfDice die1 = new PairOfDice();
        PairOfDice die2 = new PairOfDice();
        int sum;

        die1.setFaceValue1(4);
        die2.roll();
	

	System.out.print("Die one "+die1.getFaceValue1());
	System.out.println("   Die two "+die2.getFaceValue2());
	sum = die1.getFaceValue1() + die2.getFaceValue2();
	System.out.println("sum = " + sum);
}
}
