public class RollingDice
{public static void main(String[] args)
{Die die1 = new Die();
 Die die2 = new Die();
 int sum;

die1.roll();
die2.roll();
System.out.println("Die one:" + die1 + ",Die two:" + die2);

die1.roll();
die2.setfaceValue(4);
System.out.println("Die one:" + die1 + ",Die two:" + die2);

sum = die1.getfaceValue() + die2.getfaceValue();
System.out.println("Sum" + sum);

sum = die1.roll() + die2.roll();
System.out.println("Die one:" + die1 + ",Die two:" + die2);
System.out.println("Sum" + sum);
}
}


