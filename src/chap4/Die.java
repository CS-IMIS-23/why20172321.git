// Die.java   Auther:Lewis
public class Die
{
private final int MAX = 6;
private int faceValue;

public Die()
{faceValue = 1;}

public int roll()
{faceValue = (int)(Math.random() * MAX) + 1;
return faceValue;
}

public void setfaceValue(int value)
{faceValue = value;}

public int getfaceValue()
{return faceValue;}

public String toString()
{String result = Integer.toString(faceValue);
return result;}
} 
