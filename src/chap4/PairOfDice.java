//********************************************
//          PairOfDice.java       Author:why
//
//       
public class PairOfDice
{
	private final int MAX = 6;
	private int faceValue1, faceValue2;
	
//---------------------------------------------

	public PairOfDice()
	{
		faceValue1 = 1;
		faceValue2 = 1;
	}


	public void setFaceValue1(int Value1)
	{
		faceValue1 = Value1;
	}
	public int getFaceValue1()
	{
		return faceValue1;
	}

        public void setFaceValue2(int Value2)
        {
                faceValue2 = Value2;
        }
        public int getFaceValue2()
        {
                return faceValue2;
        }

//------------------------------------------------
//Rolls the die and returns the result.
//-------------------------------------------
	public int roll()
	{
	faceValue1 = (int)(Math.random() * MAX) + 1;
	return faceValue1;
	}

	public int sum()
	{
		int sum = faceValue1 + faceValue2;
		return sum;
	}
}







