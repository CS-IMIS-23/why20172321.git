import java.util.Scanner;

public class workpp212
{
    public static void main(String[] args)
    {
        double A, B, C;
 
        Scanner scan = new Scanner(System.in);

        System.out.print("输入正方形的边长: ");
        A = scan.nextDouble();
   
        B = 4 * A;
        C = A * A;
   
        System.out.println("该正方形的周长: " + B);
        System.out.println("该正方形的面积: " + C);
    }
}
