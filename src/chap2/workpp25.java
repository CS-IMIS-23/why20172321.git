import java.util.Scanner;

public class workpp25
{
      public static void main(String[] args)
   {
      Scanner scan = new Scanner(System.in);
 
      final int BASE = 32;
     
      double fahrenheitTemp;

      final double CONVERSION_FACTOR = 9.0 / 5.0;

      System.out.println("Input Celsius: ");
      double celsiusTemp = scan.nextDouble();

      fahrenheitTemp = celsiusTemp * CONVERSION_FACTOR + BASE;

      System.out.println("Celsius Temperature: " + celsiusTemp);    
      System.out.println("Fahrenheit Equivalent: " + fahrenheitTemp);
   }
}
