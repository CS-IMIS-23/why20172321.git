package week6;

public class gapSort {
    private static <T extends Comparable<T>>
    void swap(T[] data, int index1, int index2) {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    public static <T extends Comparable<T>>
    void gapSort(T[] data) {
        int position, scan;
        T temp;
        int i = 4;

        while (i >= 1) {
            for (position = data.length - 1; position >= 0; position--) {
                for (scan = 0; scan < data.length - i; scan++) {
                    if (data[scan].compareTo(data[scan + i]) > 0)
                        swap(data, scan, scan + i);
                }
            }
            i -= 2;
        }
    }
}
