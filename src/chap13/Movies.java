package chap13;

public class Movies {
    public static void main(String[] args) {
        DVDCollection movies = new DVDCollection();

        movies.add(new DVD("three body", "cixin Liu", 2006, 50.00, true));
        movies.add(new DVD("sanguo", "guangzhong Luo", 1400, 25.00, false));
        movies.add(new DVD("shisheng", "dongyeguiwu", 2010, 35.50, true));
        movies.add(new DVD("俺の妹がこんなに可愛いわけがない", "伏见司", 2008, 40.00, true));
        System.out.println(movies);
        System.out.println("-------------------------");

        movies.add(new DVD("埃罗芒阿老师", "fujiansi", 2017, 29.50, false));
        System.out.println(movies);
    }
}
