package chap13;

//*******************************************************************
//  MagazineList.java       Author: Lewis/Loftus
//
//  Represents a collection of magazines.
//*******************************************************************

public class MagazineList {
    private MagazineNode list;

    //----------------------------------------------------------------
    //  Sets up an initially empty list of magazines.
    //----------------------------------------------------------------
    public MagazineList() {
        list = null;
    }

    //----------------------------------------------------------------
    //  Creates a new MagazineNode object and adds it to the end of
    //  the linked list.
    //----------------------------------------------------------------
    public void add(Magazine mag) {
        MagazineNode node = new MagazineNode(mag);
        MagazineNode current;

        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    //在index的位置插入新节点newMagazine
    public void insert(int index, Magazine newMagazine) {
        MagazineNode node = new MagazineNode(newMagazine);
        MagazineNode current = list;
        if (index == 0) {
            list = node;
            list.next = current;
        } else {
            for (int i = 1; i < index; i++)
                current = current.next;

            node.next = current.next;
            current.next = node;
        }
    }

    //删除节点delNode
    public void delete(Magazine delNode) {
        MagazineNode current = list;
        MagazineNode pre = list;
        while (!current.magazine.equals(delNode) && current.next != null) {
            current = current.next;
            pre = current;
        }
        if (!current.magazine.equals(delNode) && current.next == null) {
            System.out.println("找不到节点，删除失败");
        } else if (current.magazine.equals(delNode) && current.next == list.next) {
            list = list.next;
        } else if (current.magazine.equals(delNode) && current.next == null) {
            pre.next = null;
        } else if (current.magazine.equals(delNode)) {
            pre.next = current.next;
        }
    }

    //----------------------------------------------------------------
    //  Returns this list of magazines as a string.
    //----------------------------------------------------------------
    public String toString() {
        String result = "";

        MagazineNode current = list;

        while (current != null) {
            result += current.magazine + "\n";
            current = current.next;
        }

        return result;
    }


    //*****************************************************************
    //  An inner class that represents a node in the magazine list.
    //  The public variables are accessed by the MagazineList class.
    //*****************************************************************
    private class MagazineNode {
        public Magazine magazine;
        public MagazineNode next;

        //--------------------------------------------------------------
        //  Sets up the node
        //--------------------------------------------------------------
        public MagazineNode(Magazine mag) {
            magazine = mag;
            next = null;
        }
    }
}

