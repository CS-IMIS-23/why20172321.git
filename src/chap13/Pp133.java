package chap13;

public class Pp133 {
    private Pp133Node list;


    public Pp133() {
        list = null;
    }


    public void add(int pp133) {
        Pp133Node node = new Pp133Node(pp133);
        Pp133Node current;

        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    public void Sorting() {
        Pp133Node min;
        int temp;
        Pp133Node current;
        Pp133Node pre = list;

        while (pre != null) {
            min = pre;
            current = min.next;
            while (current != null) {
                if (current.pp133 >= min.pp133) {
                    current = current.next;
                } else if (current.pp133 < min.pp133) {
                    min = current;
                    current = current.next;
                }
            }

            temp = min.pp133;
            min.pp133 = pre.pp133;
            pre.pp133 = temp;
            pre = pre.next;
        }
    }

    public String toString() {
        String result = "";

        Pp133Node current = list;

        while (current != null) {
            result += current.pp133 + "\n";
            current = current.next;
        }

        return result;
    }


    private class Pp133Node {
        int pp133;
        public Pp133Node next;


        public Pp133Node(int pp) {
            pp133 = pp;
            next = null;
        }
    }
}
