package chap13;

//*******************************************************************
//  MagazineRack.java       Author: Lewis/Loftus
//
//  Driver to exercise the MagazineList collection.
//*******************************************************************

public class MagazineRack {
    //----------------------------------------------------------------
    //  Creates a MagazineList object, adds several magazines to the
    //  list, then prints it.
    //----------------------------------------------------------------
    public static void main(String[] args) {
        MagazineList rack = new MagazineList();
        Magazine A = new Magazine("AAAAA");
        Magazine B = new Magazine("BBBBB");

        rack.add(new Magazine("Time"));
        rack.add(new Magazine("Woodworking Today"));
        rack.add(new Magazine("Communications of the ACM"));
        rack.add(new Magazine("House and Garden"));
        rack.add(new Magazine("GQ"));
        System.out.println("原来的:\n" + rack);

        rack.insert(0, A);
        rack.insert(3, B);
        System.out.println("插入两个后: \n" + rack);

        rack.delete(A);
        System.out.println("删除其中一个后:\n" + rack);


    }
}

