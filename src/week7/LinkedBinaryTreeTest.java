package week7;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree num1 = new LinkedBinaryTree(1);
        LinkedBinaryTree num3 = new LinkedBinaryTree(2);
        LinkedBinaryTree num4 = new LinkedBinaryTree(3);
        LinkedBinaryTree num5 = new LinkedBinaryTree(4);
        LinkedBinaryTree num6 = new LinkedBinaryTree(5, num3, num1);
        LinkedBinaryTree num7 = new LinkedBinaryTree(6, num4, num5);
        LinkedBinaryTree num2 = new LinkedBinaryTree(7, num6, num7);
        num2.toString();
        System.out.println("是否含有2:" + num2.contains(2));
        num2.removeRightSubtree();
        System.out.println();
        num2.toString();
        num2.removeAllElements();
        num2.toString();
        System.out.println("num1是叶子?" + num1.root.nodeleaf());
        System.out.println("num6是叶子?" + num6.root.nodeleaf());
    }

}
