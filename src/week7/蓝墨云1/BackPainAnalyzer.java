package week7.蓝墨云1;

import java.io.*;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to
 * diagnose back pain.
 */
public class BackPainAnalyzer {
    /**
     * Asks questions of the user to diagnose a medical problem.
     */
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("So, you're having back pain.");

        DecisionTree expert = new DecisionTree("input.txt");
        expert.evaluate();

        System.out.println("树的叶子数：" + expert.treeleaf());
        System.out.println("树的深度为：" + expert.treeheight());


        System.out.println("层序遍历：");
        expert.levelOrder1();
        System.out.println("非层序遍历：");
        expert.levelOrder2();
    }
}
