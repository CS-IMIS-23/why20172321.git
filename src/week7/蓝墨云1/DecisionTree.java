package week7.蓝墨云1;

import sun.reflect.generics.tree.Tree;
import week7.jiumingdaima.BinaryTreeNode;
import week7.jiumingdaima.LinkedBinaryTree;
import week7.书上代码.jsjf.exceptions.EmptyCollectionException;

import java.util.*;
import java.io.*;

/**
 * The DecisionTree class uses the LinkedBinaryTree class to implement
 * a binary decision tree. Tree elements are read from a given file and
 * then the decision tree can be evaluated based on user input using the
 * evaluate method.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class DecisionTree {
    private LinkedBinaryTree<String> tree;

    /**
     * Builds the decision tree based on the contents of the given file
     *
     * @param filename the name of the input file
     * @throws FileNotFoundException if the input file is not found
     */
    public DecisionTree(String filename) throws FileNotFoundException {
        File inputFile = new File(filename);
        Scanner scan = new Scanner(inputFile);
        int numberNodes = scan.nextInt();
        scan.nextLine();
        int root = 0, left, right;

        List<LinkedBinaryTree<String>> nodes = new ArrayList<LinkedBinaryTree<String>>();
        for (int i = 0; i < numberNodes; i++)
            nodes.add(i, new LinkedBinaryTree<String>(scan.nextLine()));

        while (scan.hasNext()) {
            root = scan.nextInt();
            left = scan.nextInt();
            right = scan.nextInt();
            scan.nextLine();

            nodes.set(root, new LinkedBinaryTree<String>((nodes.get(root)).getRootElement(),
                    nodes.get(left), nodes.get(right)));
        }
        tree = nodes.get(root);
    }

    /**
     * Follows the decision tree based on user responses.
     */
    public void evaluate() {
        LinkedBinaryTree<String> current = tree;
        Scanner scan = new Scanner(System.in);

        while (current.size() > 1) {
            System.out.println(current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N"))
                current = current.getLeft();
            else
                current = current.getRight();
        }

        System.out.println(current.getRootElement());
    }


    public int Treeleaf(BinaryTreeNode node) {
        if (node != null) {
            if (node.getLeft() == null && node.getRight() == null) {
                return 1;
            }
            return Treeleaf(node.getLeft()) + Treeleaf(node.getRight());
        }
        return 0;
    }

    public int treeleaf() throws EmptyCollectionException {
        BinaryTreeNode node = tree.root;
        if (node == null) {
            throw new EmptyCollectionException("TREE IS EMPTY");
        }
        return Treeleaf(node);
    }

    public int treeheight() {
        return tree.getHeight();
    }

    public void levelOrder1() {
        tree.levelOrder1(tree.root);
    }

    public void levelOrder2() {
        tree.levelOrder2();
    }

}