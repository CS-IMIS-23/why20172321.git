package week7;

import week5.LinearNode;

public class Haxichongtu {
    LinearNode[] Node;
    //创建数组
    int[] array;
    //输入个数
    int num;
    //冲突次数
    static int conflict;
    //排列次数
    int times;


    Haxichongtu(int[] a) {
        num = a.length;
        conflict = 0;
        times = 0;
        array = a;
        Node = new LinearNode[11];
    }

    LinearNode[] add() {
        for (int i = 0; i < num; i++) {
            LinearNode linearNode = new LinearNode();
            linearNode.setElement(array[i]);
            //链地址法得到位置
            int index = array[i] % 11;
            if (Node[index] == null) {
                Node[index] = linearNode;
            } else {
                LinearNode current = Node[index];
                while (current.getNext() != null) {
                    current = current.getNext();
                }
                current.setNext(linearNode);
            }
        }
        return Node;
    }


    double get() {
        double asl = 0;
        for (int i = 0; i < Node.length; i++) {
            LinearNode node = Node[i];
            int num = 1;
            while (node != null) {
                if (num >= 2)
                    conflict++;
                times = times + num;
                node = node.getNext();
                num++;
            }
        }
        asl = (double) times / num;
        return asl;
    }


    public static void main(String[] args) {
        int[] a = new int[]{11, 78, 10, 1, 3, 2, 4, 21, 42};
        Haxichongtu haxichongtu = new Haxichongtu(a);
        haxichongtu.add();
        System.out.println("ASL = " + haxichongtu.get());
        System.out.println("冲突次数：" + conflict);
    }

}
